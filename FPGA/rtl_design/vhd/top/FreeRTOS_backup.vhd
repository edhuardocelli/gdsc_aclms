library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity FreeRTOS is
	generic	(
		RES	: natural := 36;
		QRES	: natural := 18;
		PWM_MAX	: natural := 2500
	);
	port	(
		--	Clocks
		FPGA_CLK1_50, FPGA_CLK2_50, FPGA_CLK3_50	: std_logic := '0';
		--	ADC
		ADC_SDO	: in std_logic := '0';
		ADC_CONVST, ADC_SCK, ADC_SDI	: out std_logic;
		--	DEBUG
		KEY	: in unsigned(1 downto 0);
		SW	: in unsigned(3 downto 0);
		LED	: out std_logic_vector(7 downto 0);
		GPIO_0	: out std_logic_vector(35 downto 0);
		GPIO_1	: out std_logic_vector(35 downto 0)
	);
end entity FreeRTOS;

architecture beh of FreeRTOS is

	component hil_boost_avg is
		generic	(
			RES	: natural := RES;
			QRES	: natural := QRES
		);
		port 	(
			clock, enable	: in std_logic;
			v_f, v_l	: in signed(RES-1 downto 0);
			i_f, v_c, i_l	: out signed(RES-1 downto 0)
		);
	end component hil_boost_avg;

	component hil_boost is
		generic	(
			RES	: natural := RES;
			QRES	: natural := QRES
		);
		port 	(
			clock, enable	: in std_logic;
			s 	: in std_logic_vector(1 downto 0);
			v_l	: in signed(RES-1 downto 0);
			i_f, v_c, i_l	: out signed(RES-1 downto 0)
		);
	end component hil_boost;

	component ssfb is
		generic	(
			RES	: natural := RES;
			QRES	: natural := QRES;
			QRESw	: natural := 12
		);
		port	(
			clock, enable	: in std_logic;
			r, fb	: in signed(RES-1 downto 0);
			x1, x2, x3	: in signed(RES-1 downto 0);
			y 	: out signed(RES-1 downto 0);
			done	: out std_logic
		);
	end component ssfb;

	component smo is
		generic	(
			RES	: natural := RES;
			QRES	: natural := QRES
		);
		port 	(
			clock, enable	: in std_logic;
			v_f, v_l	: in signed(RES-1 downto 0);
			v_c, i_l	: in signed(RES-1 downto 0);
			i_f	: out signed(RES-1 downto 0);
			done	: out std_logic
		);
	end component smo;

	component sympwm is
		generic	(
			RES	: natural;
			MAX_COUNTER	: natural;
			INT_GEN	: natural
		);
		port	(
			clock, enable	: in std_logic;
			ref_A, ref_B	: in unsigned(RES-1 downto 0);
			pulse_A, pulse_B, intx 	: out std_logic
		);
	end component sympwm;

--	Project General Use Signals
	signal clock, enable, reset	: std_logic := '0';
	signal start, irq_pwm	: std_logic := '0';

--	Hardware Interrupt Vector (see documentation hintx.txt)
	signal hintx	: std_logic_vector(7 downto 0) := (others => '0');
	signal hcont	: std_logic_vector(3 downto 0) := (others => '0');

--	State Machine
	type fsm_st is (st_hold, st_load, st_smo, st_fb, st_end);
	signal fsm_cs, fsm_ns	: fsm_st := st_hold;

--	Boost HIL Signals
	signal s 	: std_logic_vector(1 downto 0) := (others => '0');
	type states is array(1 to 3) of signed(RES-1 downto 0);
	signal x, xr	: states := (others => (others => '0'));
	signal xe	: signed(RES-1 downto 0) := (others => '0');

	signal u, u_smo	: signed(RES-1 downto 0) := (others => '0');
	signal d	: unsigned(12 downto 0) := (others => '0');

begin

--	General Use Signals
	clock <= FPGA_CLK1_50;
	enable <= '1';
--	reset <= KEY(0);
	reset <= '1';
	led(0) <= reset;

--	Start bit (Interruption IRQ_PWM)
start_bit	: process(irq_pwm, hintx, reset)
	begin
		if hintx(1) = '1' or reset = '0' then
			start <= '0';
		elsif rising_edge(irq_pwm) then
			start <= '1';
		end if;
	end process;

--	Finite State Machine
	-- Sequential
fsm_seq	: process(fsm_ns, clock, reset)
	begin
		if reset = '0' then
			fsm_cs <= st_hold;
		elsif rising_edge(clock) then
			fsm_cs <= fsm_ns;
		end if;
	end process;

	-- Combinational
fsm_comb	: process(fsm_cs, start, hintx, hcont)
	begin
		hintx <= (others => '0');
		case fsm_cs is
			when st_hold	=>		--	Wait for Interrupt
				if start = '0' then
					fsm_ns <= st_hold;
				else
					fsm_ns <= st_load;
				end if;
			when st_load	=>		--	Load Variables (Acquisitions)
				hintx(2) <= '1';
				fsm_ns <= st_fb;
			when st_fb	=>		--	State Feedback
				hintx(4) <= '1';
				if hcont(0) = '1' then
					fsm_ns <= st_smo;
				else
					fsm_ns <= st_fb;
				end if;
			when st_smo	=>		--	Sliding Mode Observer
				hintx(3) <= '1';
				if hcont(1) = '1' then
					fsm_ns <= st_end;
				else
					fsm_ns <= st_smo;
				end if;
			when st_end	=>	--	End
				fsm_ns <= st_hold;
				hintx(1) <= '1';
			when others	=>
				fsm_ns <= st_hold;
		end case;
	end process;

--	Acquisitions
read_states	: process(clock, hintx, x, xr)
	begin
		for i in 1 to 2 loop
			if rising_edge(clock) and hintx(2) = '1' then
				xr(i) <= x(i);--shift_right(x(i) + xr(i),2);
			end if;
			xr(3) <= xe;
		end loop;
	end process;


--	Input Signals
	--u <= to_signed(262144/4, RES);
	u_smo <= resize(400*u, RES);
	d <= resize(unsigned(shift_right(1250*u, QRES)), 13);

--	Feedback (Controller)
	feedback0	: ssfb port map	(clock, hintx(4), to_signed(10*262144, RES), xr(1), xr(1), xr(2), xr(3), u, hcont(0));

--	Sliding Mode Observer
	smo0	: smo port map	(clock, hintx(3), u_smo, (others => '0'), xr(2), xr(1), xe, hcont(1));

--	Boost HIL
	--	Pulse-Width Modulator
	pwm_modulator	: sympwm
		generic map	(13, 2500, 1)
		port map	(clock, enable, d, to_unsigned(0,13), s(0), s(1), irq_pwm);
	--	Switched Model
	boost_conv	: hil_boost port map (clock, enable, s, (others => '0'), x(3), x(2), x(1));
	--	Averaged Model (Debug)
	--boostavg0	: hil_boost_avg port map (clock, hintx(3), to_signed(-131072*200, 27), (others => '0'), open, open, open);	--	Debug


	GPIO_0(0) <= irq_pwm;
	GPIO_1 <= std_logic_vector(resize((xr(1) + xr(2) + xe), GPIO_1'length));

end architecture beh;
