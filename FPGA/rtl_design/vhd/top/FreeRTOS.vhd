-----------------------------------------------------
--			Top Entity for OPAL-RT Interface		--
--
--
--
-----------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library opal;
use opal.lib_opal.all;

--library general_lib;
--use general_lib.all;

entity FreeRTOS is
	generic	(
		--	FPGA Parameters
		CLOCK_FREQUENCY	   : real := 50.0e6;
		DATA_WIDTH_DEFAULT : natural := 27;
		QN_BASE_DEFAULT	   : natural := 18;

		--	Converter Parameters
		SW_FREQUENCY	   : real := 50.0e3;--200.0*100.0;
		SAMPL_FREQUENCY	   : real := 100.0e3;--400.0*100.0;

		--	OPAL-RT Input/Output Constraints
		OPAL_WIDTH	: natural := 32;
		OPAL_WIDTH_INPUT   : natural := 16;
		OPAL_WIDTH_OUTPUT  : natural := 16;
		OPAL_CHANNEL_WIDTH : natural := 6;
		OPAL_DATA_WIDTH	 : natural := 12
	);
	port	(
		--	Clocks
		CLOCK_50	: in std_logic := '0';
		CLOCK2_50	: in std_logic := '0';
		CLOCK3_50	: in std_logic := '0';
		--	ADC
		ADC_SDO	: in std_logic := '0';
		ADC_CONVST, ADC_SCK, ADC_SDI	: out std_logic;
		--	DEBUG
		KEY	: in std_logic_vector(1 downto 0) := "11";
		SW	: in unsigned(3 downto 0);
		LED	: out std_logic_vector(7 downto 0);
		--	OPAL-RT INTERFACE
		P_IN	: in std_logic_vector(OPAL_WIDTH_OUTPUT-1 downto 0) := (others => '0');		--	OPAL-RT Output	- FPGA Input
		P_OUT	: out std_logic_vector(OPAL_WIDTH_INPUT-1 downto 0)							--	OPAL-RT Input	- FPGA Output
	);
end entity FreeRTOS;

architecture beh of FreeRTOS is

--	General Components
	--	Single Edge Detector
	component edge_detector
		generic (
			POLARITY	: std_logic;
			TIME_DELAY	: natural
		);
		port (
			CLOCK		  : in  std_logic;
			RESET		  : in  std_logic;
			ENABLE		  : in  std_logic;
			INPUT_SIGNAL  : in  std_logic;
			OUTPUT_SIGNAL : out std_logic
		);
	end component edge_detector;

	--	Both Edges Detector
	component both_edge_detector
		generic (
			POLARITY   : std_logic;
			TIME_DELAY : natural
		);
		port (
			CLOCK         : in  std_logic;
			RESET         : in  std_logic;
			ENABLE        : in  std_logic;
			INPUT_SIGNAL  : in  std_logic;
			OUTPUT_SIGNAL : out std_logic
		);
	end component both_edge_detector;

	--	Debouncer
	component debounce
		generic (
			RES           : natural;
			POLARITY      : std_logic;
			TIMEOUT       : natural;
			TIMEOUT_WIDTH : natural
		);
		port (
			CLOCK	 : in std_logic;
			RESET    : in  std_logic;
			DATA_IN  : in  std_logic_vector(RES-1 downto 0);
			DATA_OUT : out std_logic_vector(RES-1 downto 0)
		);
	end component debounce;

	component cordic
		generic (
			DATA_WIDTH : natural;
			QN_BASE    : natural;
			ITERATION  : natural
		);
		port (
			CLOCK        : in  std_logic;
			RESET        : in  std_logic;
			ENABLE       : in  std_logic;
			MODE         : in  std_logic;
			SIGNAL_IN_X  : in  signed(DATA_WIDTH-1 downto 0);
			SIGNAL_IN_Y  : in  signed(DATA_WIDTH-1 downto 0);
			SIGNAL_IN_Z  : in  signed(DATA_WIDTH-1 downto 0);
			SIGNAL_OUT_X : out signed(DATA_WIDTH-1 downto 0);
			SIGNAL_OUT_Y : out signed(DATA_WIDTH-1 downto 0);
			SIGNAL_OUT_Z : out signed(DATA_WIDTH-1 downto 0)
		);
	end component cordic;

	--	CORDIC
	component cordic_rec
		generic (
			DATA_WIDTH : natural;
			QN_BASE    : natural;
			ITERATION  : natural
		);
		port (
			CLOCK        : in  std_logic;
			RESET        : in  std_logic;
			ENABLE       : in  std_logic;
			START		 : in std_logic;
			MODE         : in  std_logic;
			SIGNAL_IN_X  : in  signed(DATA_WIDTH-1 downto 0);
			SIGNAL_IN_Y  : in  signed(DATA_WIDTH-1 downto 0);
			SIGNAL_IN_Z  : in  signed(DATA_WIDTH-1 downto 0);
			SIGNAL_OUT_X : out signed(DATA_WIDTH-1 downto 0);
			SIGNAL_OUT_Y : out signed(DATA_WIDTH-1 downto 0);
			SIGNAL_OUT_Z : out signed(DATA_WIDTH-1 downto 0)
		);
	end component cordic_rec;

	component alt_sqrt_kernel
		generic (
			INPUT_WIDTH     : natural;
			OUTPUT_WIDTH    : natural;
			PIPELINE_LENGTH : natural
		);
		port (
			CLOCK         : in  std_logic;
			INPUT_SIGNAL  : in  signed(INPUT_WIDTH-1 downto 0);
			OUTPUT_SIGNAL : out signed(OUTPUT_WIDTH-1 downto 0)
		);
	end component alt_sqrt_kernel;

	component sqrt_kernel
	generic (
	  DATA_WIDTH : natural;
	  QN_BASE    : natural;
	  ITERATION  : natural
	);
	port (
	  CLOCK        : in  std_logic;
	  RESET        : in  std_logic;
	  ENABLE       : in  std_logic;
	  START        : in  std_logic;
	  SIGNAL_IN_X  : in  signed(DATA_WIDTH-1 downto 0);
	  SIGNAL_IN_Y  : in  signed(DATA_WIDTH-1 downto 0);
	  SIGNAL_OUT_X : out signed(DATA_WIDTH-1 downto 0);
	  SIGNAL_OUT_Y : out signed(DATA_WIDTH-1 downto 0);
	  END_BIT      : out std_logic
	);
	end component sqrt_kernel;

	component sqrt_rec_kernel
		generic (
			DATA_WIDTH      : natural;
			ITERATION       : natural;
			PIPELINE_LENGTH : natural;
			PRESCALE        : natural
		);
		port (
			CLOCK     : in  std_logic;
			RESET     : in  std_logic;
			ENABLE    : in  std_logic;
			START     : in  std_logic;
			RADICAL   : in  signed(DATA_WIDTH-1 downto 0);
			RADIX     : out signed(DATA_WIDTH-1 downto 0);
			REMAINDER : out signed(DATA_WIDTH-1 downto 0);
			END_BIT   : out std_logic
		);
	end component sqrt_rec_kernel;




	function cordic_gain_func	(
		ITER	: natural;
		BASE	: natural)
	return integer is
		variable K	: real := 1.0;
	begin
		for i in 0 to ITER loop
			K := K*1.0/(sqrt(1.0+2.0**(-2*i)));
		end loop;
		return integer(floor((2.0**BASE)*K));
	end function cordic_gain_func;
	constant cordic_gain	: integer := cordic_gain_func(19,18);

--	Comverter Components

	--	Space Vector Modulation (FastSVM)
	component svm
		generic (
			DATA_WIDTH      : natural;
			CLOCK_FREQUENCY : real;
			SAMPL_FREQUENCY : real;
			QN_BASE         : natural
		);
		port (
			CLOCK     : in  std_logic;
			RESET     : in  std_logic;
			ENABLE    : in  std_logic;
			START_BIT : in  std_logic;
			SAMPLE_BIT: in std_logic;
			ALPHA     : in  signed(DATA_WIDTH-1 downto 0);
			BETA      : in  signed(DATA_WIDTH-1 downto 0);
			SWITCHES  : out std_logic_vector(2 downto 0);
			INTX      : out std_logic;
			END_SVM	  : out std_logic
		);
	end component svm;

--	OPAL-RT Components
	--	OPAL-RT Input/Output Driver
		--	Refer to PCB Design
		--	Messed up with pin order (this should be a simple fix)
	component opal_driver
		generic (
			OPAL_WIDTH_INPUT  : natural;
			OPAL_WIDTH_OUTPUT : natural
		);
		port (
			CLOCK			 : in std_logic;
			OPAL_DATAIN  : out  std_logic_vector(OPAL_WIDTH_OUTPUT-1 downto 0);
			OPAL_DATAOUT : in std_logic_vector(OPAL_WIDTH_INPUT-1 downto 0);
			P_IN         : in  std_logic_vector(OPAL_WIDTH_INPUT-1 downto 0);
			P_OUT        : out std_logic_vector(OPAL_WIDTH_OUTPUT-1 downto 0)
		);
	end component opal_driver;

	component opal_spi_protocol
	--	OPAL-RT Serial Communication (SIMO-SPI)
		generic (
			CHANNEL_WIDTH : natural;
			DATA_WIDTH    : natural
		);
		port (
			CLOCK    : in  std_logic;
			RESET    : in  std_logic;
			ENABLE   : in  std_logic;
			START    : in  std_logic;
			OPAL_CLK : in  std_logic;
			DATA_IN  : in  std_logic_vector(CHANNEL_WIDTH-1 downto 0);
			DATA_OUT : out opal_data_type(CHANNEL_WIDTH-1 downto 0);
			OPAL_CS  : out std_logic;
			END_ACQ  : out std_logic
		);
	end component opal_spi_protocol;

--	Signals

	--	Finite State Machine Signals
	type fsm_states is (IDLE, ACQUIRE, CONTROL, FSVM, FINAL, FAIL, DEBUG_UPDT);
	signal fsm_cs, fsm_ns	: fsm_states := IDLE;
	signal fault, debug_start	: std_logic := '0';

	--	Debounced Input
	signal debounced_key	: std_logic_vector(KEY'range) := (others => '0');

	--	Sampling Timebase
	signal intx_sample, start	: std_logic := '0';

	--	Interrupt Vector
	signal hintx	   : std_logic_vector(5 downto 0) := (others => '0');
	signal end_flag	   : std_logic_vector(5 downto 0) := (others => '0');

	--	Auxiliar Signals
	signal clock, reset, enable	: std_logic := '0';

	--	Opal Input/Output Signals
	signal di	: std_logic_vector(OPAL_WIDTH_INPUT-1 downto 0)  := (others => '0');
	signal do	: std_logic_vector(OPAL_WIDTH_OUTPUT-1 downto 0) := (others => '0');
	signal opal_data	: opal_data_type(OPAL_CHANNEL_WIDTH-1 downto 0) := (others => (others => '0'));

	--	Debug Signals
	signal start_spi	: std_logic := '0';
	signal ang, thet	: real := 0.0;
	signal sgn_ang 	: unsigned(1 downto 0) := (others => '0');
	signal csin, ccos	: signed(DATA_WIDTH_DEFAULT-1 downto 0) := (others => '0');
	signal alpha, beta	: signed(DATA_WIDTH_DEFAULT-1 downto 0) := (others => '0');

begin

--	Input Signals
	process
	begin
		clock <= not(clock) after 10 ns;
		wait for 10 ns;
	end process;
	--clock	<= CLOCK_50;

	rec_test	: cordic_rec
		generic map	(27, 18, 19)
		port map 	(clock, '1', '1', start, '0', to_signed(159187, 27), (others => '0'), to_signed(274517, 27), open, open, open);

	rec_test_hyp	: sqrt_kernel
		generic map	(27, 25, 26)
		port map 	(clock, '1', '1', start, to_signed(2**26*2/3 + 2**26/4, 27), to_signed(2**26*2/3 - 2**26/4, 27));

	rec_test2_hyp	: sqrt_rec_kernel
		generic map	(27, 26, 2, 2**26/4)
		port map 	(clock, '1', '1', start, to_signed(2**26*2/3, 27));


	alt_sqrt_kernel_cmp	: alt_sqrt_kernel
		generic map (26, 26, 4)
		port map	(clock, to_signed(2**26*2/3, 26), open);

	key_debounce	: debounce
		generic map	(2, '0', 30, 5)
		port map	(clock, '1', KEY, debounced_key);

	reset  <= debounced_key(0);
	enable <= debounced_key(1);

--	Start Signal (Sampling Frequency)
	irq_detection	: both_edge_detector
		generic map	('1', 3)
		port map	(clock, reset, enable, intx_sample, start);

--	Finite State Machine
	--	Combinational Logic
	FSM_seq_logic	: process(clock, reset, enable, fault, fsm_ns)
	begin
		if reset = '0' then
			fsm_cs <= IDLE;
		elsif rising_edge(clock) and enable = '1' then
			fsm_cs <= fsm_ns;
		end if;
	end process;

	--	Sequential Logic
	FSM_comb_logic	: process(fsm_cs, end_flag, start)
	begin
		--	Control Signals
		hintx <= (others => '0');
		--	Debug Signals
		debug_start <= '0';
		--	State Behaviour
		case fsm_cs is
			when IDLE 	=>
				if start = '1' then
					--fsm_ns <= ACQUIRE;
					fsm_ns <= DEBUG_UPDT;
				else
					fsm_ns <= IDLE;
				end if;

			when DEBUG_UPDT	=>
				debug_start <= '1';
				fsm_ns <= ACQUIRE;

			when ACQUIRE	=>
				hintx(0) <= '1';
				if end_flag(0) = '1' then
					fsm_ns <= CONTROL;
				else
					--fsm_ns <= ACQUIRE;
					fsm_ns <= CONTROL;
				end if;

			when CONTROL	=>
				hintx(1) <= '1';
				if end_flag(1) = '1' then
					fsm_ns <= FSVM;
				else
					fsm_ns <= CONTROL;
				end if;

			when FSVM	=>
				hintx(2) <= '1';
				if end_flag(2) = '1' then
					fsm_ns <= FINAL;
				else
					fsm_ns <= FSVM;
				end if;

			when others	=>
				fsm_ns <= IDLE;

		end case;
	end process;

	--end_flag(0) <= '1';
	end_flag(1) <= '1';
	end_flag(2) <= '1';
--	Space Vector Modulation
	fast_svm	: svm
		generic map	(DATA_WIDTH_DEFAULT, CLOCK_FREQUENCY, SAMPL_FREQUENCY, QN_BASE_DEFAULT)
		port map	(clock, reset, enable, hintx(2), debug_start, alpha, beta, open, intx_sample, open);

--	OPAL-RT Interface
	--	Driver for PCB Board
	opal_connection	: opal_driver
		generic map	(OPAL_WIDTH_INPUT, OPAL_WIDTH_OUTPUT)
		port map	(clock, do, di, P_IN, P_OUT);

	--	Debug
--	process
--	begin
--		start_spi <= '0' after 10 us;
--		start_spi <= not(start_spi) after 300 ns; wait for 300 ns;
--		start_spi <= not(start_spi) after 300 ns; wait for 300 ns;
--		start_spi <= not(start_spi) after 300 ns; wait for 300 ns;
--		start_spi <= not(start_spi) after 300 ns; wait for 300 ns;
--		start_spi <= not(start_spi) after 300 ns; wait for 300 ns;
--		start_spi <= not(start_spi) after 300 ns; wait for 300 ns;
--		start_spi <= not(start_spi) after 300 ns; wait for 300 ns;
--		start_spi <= not(start_spi) after 300 ns; wait for 300 ns;
--		start_spi <= not(start_spi) after 300 ns; wait for 300 ns;
--		start_spi <= not(start_spi) after 300 ns; wait for 300 ns;
--		start_spi <= not(start_spi) after 300 ns; wait for 300 ns;
--		start_spi <= not(start_spi) after 300 ns; wait for 300 ns;
--		start_spi <= not(start_spi) after 300 ns; wait for 300 ns;
--		start_spi <= '0' after 22.2 us;
--	end process;

	--	SPI Protocol
	opal_spi	: opal_spi_protocol
		generic map	(OPAL_CHANNEL_WIDTH, OPAL_DATA_WIDTH)
		port map 	(clock, reset, enable, hintx(0), do(4), do(13 downto 8), opal_data, di(8), end_flag(0));

	alpha <= signed(shift_left(resize(unsigned(opal_data(0)), DATA_WIDTH_DEFAULT), QN_BASE_DEFAULT-OPAL_DATA_WIDTH+1)) - 2**(QN_BASE_DEFAULT-1);
	beta  <= signed(shift_left(resize(unsigned(opal_data(1)), DATA_WIDTH_DEFAULT), QN_BASE_DEFAULT-OPAL_DATA_WIDTH+1)) - 2**(QN_BASE_DEFAULT-1);

	--	Debug Alpha-Beta
--	process(clock, debug_start, ang)
--	begin
--		if rising_edge(clock) and debug_start = '1' then
--			alpha <= to_signed(integer(round(2.0**17.0*cos(ang))), DATA_WIDTH_DEFAULT);
--			beta <= to_signed(integer(round(2.0**17.0*sin(ang))), DATA_WIDTH_DEFAULT);
--			if ang < 2*3.1415926538 then
--				ang <= ang + 2*3.1415926538*50.0/40.0e3;
--			else
--				ang <= ang - 2*3.1415926538 + 2*3.1415926538*50.0/40.0e3;
--			end if;
--		end if;
--	end process;

end architecture beh;
