library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity hil_boost is
	generic	(
		RES	: natural := 27;
		QRES	: natural := 18
	);
	port 	(
		clock, enable	: in std_logic;
		s 	: in std_logic_vector(1 downto 0);
		v_l	: in signed(RES-1 downto 0);
		i_f, v_c, i_l	: out signed(RES-1 downto 0)
	);
end entity hil_boost;

architecture hil_beh of hil_boost is

	type matrix is array(1 to 3, 1 to 3) of integer range -134217728 to 134217727;
	type signed_mx is array(1 to 3, 1 to 3) of signed(RES-1 downto 0);
	type signed_vec is array(1 to 3) of signed(RES-1 downto 0);

	function eval_At return signed_mx is
		variable At	: signed_mx;
		constant A	: matrix := ((261768, 37, 2), (-265, 262144, 265), (2, -37, 262140));	-- Impulse
	begin
		for i in 1 to 3 loop
			for j in 1 to 3 loop
				At(i,j) := to_signed(A(i,j), RES);
			end loop;
		end loop;
		return At;
	end function eval_At;

	constant At	: signed_mx	:= eval_At;
	constant Bt	: signed_vec	:= (to_signed(0, RES), to_signed(8, RES), to_signed(14980, RES));	-- Euler

	function smult_qn	(
		u, v 	: signed;
		Qn	: natural
	)	return signed is
	begin
		return resize(shift_right(u*v, Qn), u'length);
	end function smult_qn;

	function mult_mxv	(
		A	: signed_mx;
		v	: signed_vec
	)	return signed_vec is
		variable y	: signed_vec;
	begin
		for i in 1 to 3 loop
			y(i) := smult_qn(A(i,1),v(1),QRES) + smult_qn(A(i,2),v(2),QRES) + smult_qn(A(i,3),v(3),QRES);
		end loop;
		return y;
	end function mult_mxv;

	function mult_gainvec	(
		v	: signed_vec;
		K	: signed
	)	return signed_vec is
		variable y	: signed_vec;
	begin
		for i in 1 to 3 loop
			y(i) := smult_qn(v(i), k, QRES);
		end loop;
		return y;
	end function mult_gainvec;

	function sum_vec	(
		u, v	: signed_vec
	)	return signed_vec is
		variable w	: signed_vec;
	begin
		for i in 1 to 3 loop
			w(i) := u(i) + v(i);
		end loop;
		return w;
	end function sum_vec;

	function sub_vec	(
		u, v	: signed_vec
	)	return signed_vec is
		variable w	: signed_vec;
	begin
		for i in 1 to 3 loop
			w(i) := u(i) - v(i);
		end loop;
		return w;
	end function sub_vec;


--	constant Bt	: signed_vec	:= (to_signed(3182, RES), to_signed(54823, RES), to_signed(40957, RES));	-- Impulse
	signal x	: signed_vec	:= (others => (others => '0'));
	signal u, w	: signed(RES-1 downto 0) := (others => '0');

begin

	--u <= v_f;

	process(clock, enable, x, u, s)
	begin
		if rising_edge(clock) and enable = '1' then
			if s = "10" then
				x <= sum_vec(mult_mxv(At,x), Bt);
			elsif s = "01" then
				--x <= sub_vec(mult_mxv(At,x), Bt);
				x <= sum_vec(mult_mxv(At,x), Bt);
			else
				x <= mult_mxv(At,x);
			end if;
		end if;
	end process;

	i_l <= x(1);
	v_c <= x(2);
	i_f <= x(3);

end architecture hil_beh;
