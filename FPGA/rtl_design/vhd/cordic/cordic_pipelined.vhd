library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity cordic is
	generic	(
		DATA_WIDTH	: natural;
		QN_BASE		: natural;
		ITERATION	: natural
	);
	port 	(
		CLOCK		 : in std_logic;
		RESET		 : in std_logic;
		ENABLE		 : in std_logic;
		MODE		 : in std_logic;
		SIGNAL_IN_X	 : in signed(DATA_WIDTH-1 downto 0);
		SIGNAL_IN_Y	 : in signed(DATA_WIDTH-1 downto 0);
		SIGNAL_IN_Z	 : in signed(DATA_WIDTH-1 downto 0);
		SIGNAL_OUT_X : out signed(DATA_WIDTH-1 downto 0);
		SIGNAL_OUT_Y : out signed(DATA_WIDTH-1 downto 0);
		SIGNAL_OUT_Z : out signed(DATA_WIDTH-1 downto 0)
	);
end entity cordic;

architecture beh of cordic is

	type debug is array(0 to 2) of signed(SIGNAL_IN_X'range);
	type cordic_iteration_vector is array(0 to iteration, 0 to 2) of signed(SIGNAL_IN_X'range);
	type rotation_angle is array(0 to ITERATION-1) of integer;
	type cordic_coordinate is array(0 to ITERATION) of signed(SIGNAL_IN_X'range);

	component cordic_it
		generic (
			DATA_WIDTH       : natural;
			ITERATION_NUMBER : natural
		);
		port (
			MODE     : in  std_logic;
			ANGLE    : in  integer;
			INPUT_X  : in  signed(DATA_WIDTH-1 downto 0);
			INPUT_Y  : in  signed(DATA_WIDTH-1 downto 0);
			INPUT_Z  : in  signed(DATA_WIDTH-1 downto 0);
			OUTPUT_X : out signed(DATA_WIDTH-1 downto 0);
			OUTPUT_Y : out signed(DATA_WIDTH-1 downto 0);
			OUTPUT_Z : out signed(DATA_WIDTH-1 downto 0)
		);
	end component cordic_it;

	function assign_angles	(
		iteration	: natural;
		base	: natural
	)	return rotation_angle is
		variable mem	: rotation_angle;
	begin
		for i in 0 to iteration-1 loop
			mem(i) := integer((2.0**base)*arctan(2.0**(real(-i))));--integer((2.0**real(base)) * atan(1.0/(2.0**(1.0*real(i)))));
		end loop;
		return mem;
	end function assign_angles;

	signal sgd	: debug := (others => (others => '0'));
	signal sg	: cordic_iteration_vector := (others => (others => (others => '0')));
	signal sigma	: std_logic_vector(ITERATION-1 downto 0) := (others => '0');
	signal x,y,z	: cordic_coordinate := (others => (others => '0'));

	constant rot_angle_table	: rotation_angle := assign_angles(ITERATION, QN_BASE);

begin

--	Input Vector
	x(0) <= SIGNAL_IN_X;
	y(0) <= SIGNAL_IN_Y;
	z(0) <= SIGNAL_IN_Z;

	alg_it	: for i in 0 to ITERATION-1 generate
		c_it	: cordic_it
			generic map	(SIGNAL_IN_X'length, i)
			port map	(MODE, rot_angle_table(i), x(i), y(i), z(i), x(i+1), y(i+1), z(i+1));
	end generate;

	process(CLOCK, RESET, ENABLE, x, y, z)
	begin
		if RESET = '0' then
			SIGNAL_OUT_X <= (others => '0');
			SIGNAL_OUT_Y <= (others => '0');
			SIGNAL_OUT_Z <= (others => '0');
		elsif rising_edge(CLOCK) and ENABLE = '1' then
			SIGNAL_OUT_X <= x(x'length-1);
			SIGNAL_OUT_Y <= y(y'length-1);
			SIGNAL_OUT_Z <= z(z'length-1);
		end if;
	end process;


end architecture beh;
