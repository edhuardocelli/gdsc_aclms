library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity sqrt_rec_kernel is
	generic	(
		DATA_WIDTH		: natural;
		ITERATION		: natural;
		PIPELINE_LENGTH	: natural;
		PRESCALE		: natural
	);
	port 	(
		CLOCK		 : in std_logic;
		RESET		 : in std_logic;
		ENABLE		 : in std_logic;
		START		 : in std_logic;
		RADICAL	 	 : in signed(DATA_WIDTH-1 downto 0);
		RADIX 		 : out signed(DATA_WIDTH-1 downto 0);
		REMAINDER 	 : out signed(DATA_WIDTH-1 downto 0);
		END_BIT		 : out std_logic
	);
end entity sqrt_rec_kernel;

architecture beh of sqrt_rec_kernel is

	type cordic_coordinate is array(0 to PIPELINE_LENGTH) of signed(RADICAL'range);

	component sqrt_kernel_it
		generic (
			DATA_WIDTH : natural
		);
		port (
			ITERATION_NUMBER : natural range 0 to 31;
			INPUT_X          : in  signed(DATA_WIDTH-1 downto 0);
			INPUT_Y          : in  signed(DATA_WIDTH-1 downto 0);
			OUTPUT_X         : out signed(DATA_WIDTH-1 downto 0);
			OUTPUT_Y         : out signed(DATA_WIDTH-1 downto 0)
		);
	end component sqrt_kernel_it;

	--	Finite State Machine Signals
	type fsm_states is (IDLE, LOAD, ITERATE, FINAL);
	signal fsm_cs, fsm_ns	 : fsm_states := IDLE;
	signal hintx	: std_logic_vector(2 downto 0) := (others => '0');

	--	Iteration Signals
	signal x,y	: cordic_coordinate := (others => (others => '0'));

	--	Multiplexer iteration number
	type iteration_vector is array(0 to 31) of natural range 0 to 31;
	signal iteration_counter	: natural range 0 to 31:= 0;
	signal kernel_it	: iteration_vector := (others => 0);

	--	Iteration # vector (repeating iterations 4, 13, 40, ...)
	function assign_iter	(
		iteration	: natural
	)	return iteration_vector is
		variable it	: iteration_vector := (others => 0);
		variable k	: natural := 4;
		variable j	: integer := 0;
	begin
		for i in 0 to 31 loop
			if i+j = k then
				k := 3*(k) + 1;
				j := j-1;
			end if;
			it(i) := i+j+1;
		end loop;
		return it;
	end function assign_iter;

	constant iteration_number 	: iteration_vector := assign_iter(ITERATION);

begin

	process(CLOCK, fsm_ns)
	begin
		if rising_edge(CLOCK) then
			fsm_cs <= fsm_ns;
		end if;
	end process;

	process(fsm_cs, START, iteration_counter)
	begin
		hintx <= (others => '0');
		case fsm_cs is
			when IDLE	=>
				if START = '1' then
					fsm_ns <= LOAD;
				else
					fsm_ns <= IDLE;
				end if;

			when LOAD	=>
				hintx(0) <= '1';
				hintx(1) <= '1';
				fsm_ns <= ITERATE;

			when ITERATE	=>
				hintx(1) <= '1';
				if iteration_counter = ITERATION-2*PIPELINE_LENGTH then
					fsm_ns <= FINAL;
				else
					fsm_ns <= ITERATE;
				end if;

			when FINAL	=>
				hintx(1) <= '1';
				hintx(2) <= '1';
				fsm_ns <= IDLE;

			when others	=>
				fsm_ns <= IDLE;
		end case;
	end process;

	process(iteration_counter)
	begin
		for i in 0 to PIPELINE_LENGTH-1 loop
			kernel_it(i) <= iteration_number(iteration_counter + i);
		end loop;
	end process;

	kernel_gen	: for i in 0 to PIPELINE_LENGTH-1 generate
		it	: sqrt_kernel_it
			generic map	(DATA_WIDTH)
			port map	(kernel_it(i), x(i), y(i), x(i+1), y(i+1));
	end generate kernel_gen;

	process(CLOCK, hintx, iteration_counter)
	begin
		if rising_edge(CLOCK) then
			if hintx(0) = '1' or hintx(2) = '1' then
				iteration_counter <= 0;
			else
				if hintx(1) = '1' then
					iteration_counter <= iteration_counter + PIPELINE_LENGTH;
				end if;
			end if;
		end if;
	end process;

	process(CLOCK, RADICAL, x, y, hintx, iteration_counter)
	begin
		if rising_edge(CLOCK) then
			if hintx(0) = '1' then
				x(0) <= RADICAL + PRESCALE;
				y(0) <= RADICAL - PRESCALE;
			else
				if hintx(1) = '1' then
					x(0) <= x(PIPELINE_LENGTH);
					y(0) <= y(PIPELINE_LENGTH);
				end if;
			end if;
		end if;
	end process;

	RADIX <= x(0);
	REMAINDER <= y(0);

	END_BIT <= hintx(2);

end architecture beh;
