library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity sqrt_kernel_it is
	generic	(
		DATA_WIDTH		 : natural
		);
	port	(
		ITERATION_NUMBER : natural range 0 to 31;
		INPUT_X	 : in signed(DATA_WIDTH-1 downto 0);
		INPUT_Y	 : in signed(DATA_WIDTH-1 downto 0);
		OUTPUT_X : out signed(DATA_WIDTH-1 downto 0);
		OUTPUT_Y : out signed(DATA_WIDTH-1 downto 0)
	);
end entity sqrt_kernel_it;

architecture beh of sqrt_kernel_it is

	signal sigma	: std_logic := '0';

begin
	--sigma <= ;
	rotation_beh	: process(INPUT_X, INPUT_Y, ITERATION_NUMBER, sigma)
	begin
		if INPUT_Y(INPUT_Y'length-1) = '1' then		--	Counter-clockwise rotation
			OUTPUT_X <= INPUT_X + shift_right(INPUT_Y, ITERATION_NUMBER);
			OUTPUT_Y <= INPUT_Y + shift_right(INPUT_X, ITERATION_NUMBER);
		else 										--	Clockwise rotation
			OUTPUT_X <= INPUT_X - shift_right(INPUT_Y, ITERATION_NUMBER);
			OUTPUT_Y <= INPUT_Y - shift_right(INPUT_X, ITERATION_NUMBER);
		end if;
	end process;

end architecture beh;
