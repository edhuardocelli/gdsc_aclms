library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity sqrt_kernel is
	generic	(
		DATA_WIDTH	: natural;
		QN_BASE		: natural;
		ITERATION	: natural
	);
	port 	(
		CLOCK		 : in std_logic;
		RESET		 : in std_logic;
		ENABLE		 : in std_logic;
		START		 : in std_logic;
		SIGNAL_IN_X	 : in signed(DATA_WIDTH-1 downto 0);
		SIGNAL_IN_Y	 : in signed(DATA_WIDTH-1 downto 0);
		SIGNAL_OUT_X : out signed(DATA_WIDTH-1 downto 0);
		SIGNAL_OUT_Y : out signed(DATA_WIDTH-1 downto 0);
		END_BIT		 : out std_logic
	);
end entity sqrt_kernel;

architecture beh of sqrt_kernel is

	type cordic_iteration_vector is array(0 to iteration, 0 to 2) of signed(SIGNAL_IN_X'range);
	type rotation_angle is array(0 to ITERATION) of integer;
	type cordic_coordinate is array(0 to 1) of signed(SIGNAL_IN_X'range);

	component sqrt_kernel_it
		generic (
			DATA_WIDTH : natural
		);
		port (
			ITERATION_NUMBER : natural range 0 to 31;
			INPUT_X          : in  signed(DATA_WIDTH-1 downto 0);
			INPUT_Y          : in  signed(DATA_WIDTH-1 downto 0);
			OUTPUT_X         : out signed(DATA_WIDTH-1 downto 0);
			OUTPUT_Y         : out signed(DATA_WIDTH-1 downto 0)
		);
	end component sqrt_kernel_it;

	--	Finite State Machine Signals
	type fsm_states is (IDLE, LOAD, ITERATE, FINAL);
	signal fsm_cs, fsm_ns	 : fsm_states := IDLE;
	signal hintx	: std_logic_vector(2 downto 0) := (others => '0');
	--	Iteration Signals
	signal sigma	: std_logic_vector(ITERATION-1 downto 0) := (others => '0');
	signal x,y,z	: cordic_coordinate := (others => (others => '0'));
	signal iteration_counter, kernel_it	: natural range 0 to 31:= 0;
	type iteration_vector is array(0 to ITERATION) of natural range 0 to 31;

	function assign_iter	(
		iteration	: natural
	)	return iteration_vector is
		variable it	: iteration_vector := (others => 0);
		variable k	: natural := 4;
		variable j	: integer := 0;
	begin
		for i in 0 to iteration loop
			if i+j = k then
				k := 3*(k) + 1;
				j := j-1;
			end if;
			it(i) := i+j+1;
		end loop;
		return it;
	end function assign_iter;

	constant iteration_number 	: iteration_vector := assign_iter(ITERATION);

begin

	process(CLOCK, fsm_ns)
	begin
		if rising_edge(CLOCK) then
			fsm_cs <= fsm_ns;
		end if;
	end process;

	process(fsm_cs, START, iteration_counter)
	begin
		hintx <= (others => '0');
		case fsm_cs is
			when IDLE	=>
				if START = '1' then
					fsm_ns <= LOAD;
				else
					fsm_ns <= IDLE;
				end if;

			when LOAD	=>
				hintx(0) <= '1';
				hintx(1) <= '1';
				fsm_ns <= ITERATE;

			when ITERATE	=>
				hintx(1) <= '1';
				if iteration_counter = ITERATION-2 then
					fsm_ns <= FINAL;
				else
					fsm_ns <= ITERATE;
				end if;

			when FINAL	=>
				hintx(1) <= '1';
				hintx(2) <= '1';
				fsm_ns <= IDLE;

			when others	=>
				fsm_ns <= IDLE;
		end case;
	end process;

	kernel_it <= iteration_number(iteration_counter);

	it0	: sqrt_kernel_it
		generic map	(DATA_WIDTH)
		port map	(kernel_it, x(0), y(0), x(1), y(1));

	process(CLOCK, hintx, iteration_counter)
	begin
		if rising_edge(CLOCK) then
			if hintx(0) = '1' or hintx(2) = '1' then
				iteration_counter <= 0;
			else
				if hintx(1) = '1' then
					iteration_counter <= iteration_counter + 1;
				end if;
			end if;
		end if;
	end process;

	process(CLOCK, SIGNAL_IN_X, SIGNAL_IN_Y, x, y, hintx, iteration_counter)
	begin
		if rising_edge(CLOCK) then
			if hintx(0) = '1' then
				x(0) <= SIGNAL_IN_X;
				y(0) <= SIGNAL_IN_Y;
			else
				if hintx(1) = '1' then
					x(0) <= x(1);
					y(0) <= y(1);
				end if;
			end if;
		end if;
	end process;

	SIGNAL_OUT_X <= x(0);
	SIGNAL_OUT_Y <= y(0);

	END_BIT <= hintx(2);

end architecture beh;
