library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity cordic_rec is
	generic	(
		DATA_WIDTH	: natural;
		QN_BASE		: natural;
		ITERATION	: natural;
		MODE		: std_logic
	);
	port 	(
		CLOCK		 : in std_logic;
		RESET		 : in std_logic;
		ENABLE		 : in std_logic;
		START		 : in std_logic;
		SIGNAL_IN_X	 : in signed(DATA_WIDTH-1 downto 0);
		SIGNAL_IN_Y	 : in signed(DATA_WIDTH-1 downto 0);
		SIGNAL_IN_Z	 : in signed(DATA_WIDTH-1 downto 0);
		SIGNAL_OUT_X : out signed(DATA_WIDTH-1 downto 0);
		SIGNAL_OUT_Y : out signed(DATA_WIDTH-1 downto 0);
		SIGNAL_OUT_Z : out signed(DATA_WIDTH-1 downto 0);
		END_BIT		 : out std_logic
	);
end entity cordic_rec;

architecture beh of cordic_rec is

	type debug is array(0 to 2) of signed(SIGNAL_IN_X'range);
	type cordic_iteration_vector is array(0 to iteration, 0 to 2) of signed(SIGNAL_IN_X'range);
	type rotation_angle is array(0 to ITERATION) of integer;
	type cordic_coordinate is array(0 to 1) of signed(SIGNAL_IN_X'range);

	component cordic_it_rec
		generic (
			DATA_WIDTH       : natural;
			MODE     : in  std_logic
		);
		port (
			ANGLE    : in  integer;
			ITERATION_NUMBER : natural range 0 to 31;
			INPUT_X  : in  signed(DATA_WIDTH-1 downto 0);
			INPUT_Y  : in  signed(DATA_WIDTH-1 downto 0);
			INPUT_Z  : in  signed(DATA_WIDTH-1 downto 0);
			OUTPUT_X : out signed(DATA_WIDTH-1 downto 0);
			OUTPUT_Y : out signed(DATA_WIDTH-1 downto 0);
			OUTPUT_Z : out signed(DATA_WIDTH-1 downto 0)
		);
	end component cordic_it_rec;

	function assign_angles	(
		iteration	: natural;
		base	: natural
	)	return rotation_angle is
		variable mem	: rotation_angle;
	begin
		for i in 0 to iteration loop
			mem(i) := integer((2.0**base)*arctan(2.0**(real(-i))));--integer((2.0**real(base)) * atan(1.0/(2.0**(1.0*real(i)))));
		end loop;
		return mem;
	end function assign_angles;

	type fsm_states is (IDLE, LOAD, ITERATE, FINAL);
	signal fsm_cs, fsm_ns	 : fsm_states := IDLE;

	signal sgd	: debug := (others => (others => '0'));
	signal sg	: cordic_iteration_vector := (others => (others => (others => '0')));
	signal sigma	: std_logic_vector(ITERATION-1 downto 0) := (others => '0');
	signal x,y,z	: cordic_coordinate := (others => (others => '0'));
	signal iteration_number	: natural range 0 to 31 := 0;
	constant rot_angle_table	: rotation_angle := assign_angles(ITERATION, QN_BASE);
	signal theta	: integer := 0;
	signal hintx	: std_logic_vector(2 downto 0) := (others => '0');

begin

	process(CLOCK, fsm_ns)
	begin
		if rising_edge(CLOCK) then
			fsm_cs <= fsm_ns;
		end if;
	end process;

	process(fsm_cs, START, iteration_number)
	begin
		hintx <= (others => '0');
		case fsm_cs is
			when IDLE	=>
				if START = '1' then
					fsm_ns <= LOAD;
				else
					fsm_ns <= IDLE;
				end if;

			when LOAD	=>
				hintx(0) <= '1';
				fsm_ns <= ITERATE;

			when ITERATE	=>
				hintx(1) <= '1';
				if iteration_number = ITERATION-2 then
					fsm_ns <= FINAL;
				else
					fsm_ns <= ITERATE;
				end if;

			when FINAL	=>
				hintx(2) <= '1';
				fsm_ns <= IDLE;

			when others	=>
				fsm_ns <= IDLE;
		end case;
	end process;


	theta <= rot_angle_table(iteration_number);
	it0	: cordic_it_rec
		generic map	(DATA_WIDTH, '1')
		port map	(theta, iteration_number, x(0), y(0), z(0), x(1), y(1), z(1));

	process(CLOCK, SIGNAL_IN_X, SIGNAL_IN_Y, SIGNAL_IN_Z, x, y, z, hintx, iteration_number)
	begin
		if rising_edge(CLOCK) then
			if hintx(0) = '1' then
				iteration_number <= 0;
				x(0) <= SIGNAL_IN_X;
				y(0) <= SIGNAL_IN_Y;
				z(0) <= SIGNAL_IN_Z;
			else
				if hintx(1) = '1' then
					iteration_number <= iteration_number + 1;
					x(0) <= x(1);
					y(0) <= y(1);
					z(0) <= z(1);
				end if;
			end if;
		end if;
	end process;

	SIGNAL_OUT_X <= x(1);
	SIGNAL_OUT_Y <= y(1);
	SIGNAL_OUT_Z <= z(1);

	END_BIT <= hintx(2);

end architecture beh;
