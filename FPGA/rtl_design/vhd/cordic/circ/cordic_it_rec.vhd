library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity cordic_it_rec is
	generic	(
		DATA_WIDTH		 : natural;
		MODE	 : in std_logic
		);
	port	(
		ANGLE	 : in integer;
		ITERATION_NUMBER : natural range 0 to 31;
		INPUT_X	 : in signed(DATA_WIDTH-1 downto 0);
		INPUT_Y	 : in signed(DATA_WIDTH-1 downto 0);
		INPUT_Z	 : in signed(DATA_WIDTH-1 downto 0);
		OUTPUT_X : out signed(DATA_WIDTH-1 downto 0);
		OUTPUT_Y : out signed(DATA_WIDTH-1 downto 0);
		OUTPUT_Z : out signed(DATA_WIDTH-1 downto 0)
	);
end entity cordic_it_rec;

architecture beh of cordic_it_rec is

	signal sigma	: std_logic := '0';

begin

	rot_direction	: process(INPUT_X, INPUT_Y, INPUT_Z)
	begin
		if MODE = '0' then	--	Rotation Mode
			sigma <= INPUT_Z(INPUT_X'length-1);
		else				--	Vectoring Mode
			sigma <= not(INPUT_X(INPUT_X'length-1) xor INPUT_Y(INPUT_Y'length-1));
		end if;
	end process;

	rotation_beh	: process(INPUT_X, INPUT_Y, INPUT_Z, ANGLE, sigma)
	begin
		if sigma = '0' then		--	Counter-clockwise rotation
			OUTPUT_X <= INPUT_X - shift_right(INPUT_Y, ITERATION_NUMBER);
			OUTPUT_Y <= INPUT_Y + shift_right(INPUT_X, ITERATION_NUMBER);
			OUTPUT_Z <= INPUT_Z - ANGLE;
		else					--	Clockwise rotation
			OUTPUT_X <= INPUT_X + shift_right(INPUT_Y, ITERATION_NUMBER);
			OUTPUT_Y <= INPUT_Y - shift_right(INPUT_X, ITERATION_NUMBER);
			OUTPUT_Z <= INPUT_Z + ANGLE;
		end if;
	end process;

end architecture beh;
