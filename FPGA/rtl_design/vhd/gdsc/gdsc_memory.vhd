library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity gdsc_memory is
	generic	(
		MEMORY_WIDTH : natural;
		DATA_WIDTH	 : natural
	);
	port	(
		CLOCK	 		 : in std_logic;
		RESET	 		 : in std_logic;
		ENABLE	 		 : in std_logic;
		DATA_IN	 		 : in signed(DATA_WIDTH-1 downto 0);
		DATA_OUT_CURRENT : out signed(DATA_WIDTH-1 downto 0);
		DATA_OUT_DELAYED : out signed(DATA_WIDTH-1 downto 0)
	);
end entity gdsc_memory;

architecture beh of gdsc_memory is

	type memory_array is array(0 to MEMORY_WIDTH) of signed(DATA_WIDTH-1 downto 0);
	signal memory	: memory_array := (others => (others => '0'));

begin

	mem_beh	: process(CLOCK, RESET, ENABLE, DATA_IN, memory)
	begin
		if RESET = '0' then
			memory <= (others => (others => '0'));
		elsif rising_edge(CLOCK) and ENABLE = '1' then
			for i in 0 to memory'length-2 loop
				memory(i+1) <= memory(i);
			end loop;
			memory(0) <= DATA_IN;
		end if;
	end process;

	DATA_OUT_CURRENT <= memory(0);
	DATA_OUT_DELAYED <= memory(memory'length-1);

end architecture beh;
