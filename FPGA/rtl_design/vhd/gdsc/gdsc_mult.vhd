library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity gdsc_mult is
	generic	(
		DATA_WIDTH	: natural;
		QN_BASE		: natural;
		STAGE_LENGTH	: natural
	);
	port	(
		CLOCK	: in std_logic;
		RESET	: in std_logic;
		ENABLE	: in std_logic;
		START_BIT	: in std_logic;
		SIGNAL_ALPHA	: in signed(DATA_WIDTH-1 downto 0);
		SIGNAL_BETA		: in signed(DATA_WIDTH-1 downto 0);
		FILTERED_ALPHA	: out signed(DATA_WIDTH-1 downto 0);
		FILTERED_BETA	: out signed(DATA_WIDTH-1 downto 0);
		END_BIT	: out std_logic
	);
end entity gdsc_mult;

architecture beh of gdsc_mult is

--	Type Declaration for Constants and Signals
	type volt_vector is array(0 to 1) of signed(SIGNAL_ALPHA'range);
	type lin_transf_mx is array(0 to 1, 0 to 1) of signed(SIGNAL_ALPHA'range);
	type gdsc_transf_mx is array(0 to STAGE_LENGTH-1) of lin_transf_mx;

	function smult_qn	(
		u, v : signed;
		Qn	 : natural
	)	return signed is
	begin
		return resize(shift_right(u*v, Qn), u'length);
	end function smult_qn;

	function lin_transf_gdsc	(
		u 		: volt_vector;
		T		: lin_transf_mx;
		Qn_base	: natural
	)	return volt_vector is
		variable y	: volt_vector
	begin
		for i in u'range loop
			y(i) := (others => '0');
			for j in u'range loop
				y(i) := y(i) + smult_qn(T(i,j), u(j), Qn_base);
			end loop;
		end loop;
		return y;
	end function lin_transf_gdsc;

	function lin_transf_gdsc_reduced	(
		u 	: volt_vector;



begin

end architecture beh;
