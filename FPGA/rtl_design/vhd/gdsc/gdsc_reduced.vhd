library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity gdsc_reduced is
	generic	(
		DATA_WIDTH	: natural;
		QN_BASE		: natural;
		STAGE_LENGTH	: natural
	);
	port	(
		CLOCK	: in std_logic;
		RESET	: in std_logic;
		ENABLE	: in std_logic;
		START_BIT	: in std_logic;
		SIGNAL_ALPHA	: in signed(DATA_WIDTH-1 downto 0);
		SIGNAL_BETA		: in signed(DATA_WIDTH-1 downto 0);
		FILTERED_ALPHA	: out signed(DATA_WIDTH-1 downto 0);-- := (others => '0');
		FILTERED_BETA	: out signed(DATA_WIDTH-1 downto 0);-- := (others => '0');
		END_BIT	: out std_logic
	);
end entity gdsc_reduced;

architecture beh of gdsc_reduced is

--	Type Declaration for Constants and Signals
	type volt_vector is array(0 to 1) of signed(SIGNAL_ALPHA'range);
	type lin_transf_vec is array(0 to STAGE_LENGTH-1, 0 to 1) of real;

--	Functions
	--	Functions for Matrix Operations
	function si_mult_qn	(
		u 	: signed;
		v	: integer;
		Qn	: natural
	)	return signed is
	begin
		return resize(shift_right(u*v, Qn), u'length);
	end function si_mult_qn;

	function lin_transf_gdsc_red	(
		u 		: volt_vector;
		b		: lin_transf_vec;
		qn_base	: natural;
		iter	: natural
	)	return volt_vector is
		variable y, y1, y2	: volt_vector := (others => (others => '0'));
	begin
		for i in u'range loop
			y1(i) := si_mult_qn(u(i), integer(round(2.0**qn_base*b(iter,i))), qn_base);
			y2(i) := si_mult_qn(u(i), integer(round(2.0**qn_base*b(iter,(1-i)))), qn_base);
		end loop;
		y(0) := y1(0) - y1(1);
		y(1) := y2(0) + y2(1);
		return y;
	end function lin_transf_gdsc_red;

	function srr_volt_vect	(
		u 	: volt_vector;
		n	: natural
	)	return volt_vector is
		variable y	: volt_vector := (others => (others => '0'));
	begin
		for i in u'range loop
			y(i) := shift_right(u(i), n);
		end loop;
		return y;
	end function srr_volt_vect;

	function "+"	(
		u, v	: volt_vector
	)	return volt_vector is
		variable y	: volt_vector := (others => (others => '0'));
	begin
		for i in u'range loop
			y(i) := u(i) + v(i);
		end loop;
		return y;
	end function "+";
--	Components
	--	Memory
	component gdsc_memory
		generic (
			MEMORY_WIDTH : natural;
			DATA_WIDTH   : natural
		);
		port (
			CLOCK    : in  std_logic;
			RESET    : in  std_logic;
			ENABLE   : in  std_logic;
			DATA_IN  : in  signed(DATA_WIDTH-1 downto 0);
			DATA_OUT_CURRENT : out signed(DATA_WIDTH-1 downto 0);
			DATA_OUT_DELAYED : out signed(DATA_WIDTH-1 downto 0)
		);
	end component gdsc_memory;

--	Signals
	--	Finite State Machine Signals
	type fsm_states is (IDLE, LOAD, ITERATE, FINAL);
	signal fsm_cs, fsm_ns	: fsm_states := IDLE;
	signal hintx, flag	: std_logic_vector(4 downto 0) := (others => '0');

	--	Memory Related Signals
	signal mem_push	: std_logic_vector(STAGE_LENGTH downto 0) := (others => '0');
	signal iter_number	: integer range 0 to STAGE_LENGTH-1 := 0;

	--	Filter Stage vector
	type filter_vector is array(0 to STAGE_LENGTH-1) of volt_vector;
	constant b	: lin_transf_vec := ((-0.5, 0.0), (0.0, 0.5), (0.353553390593274, 0.353553390593274), (0.461939766255643, 0.191341716182545), (0.490392640201615, 0.097545161008064));
	type delay_vector is array(0 to STAGE_LENGTH-1) of natural;
	constant p	: delay_vector := (1600, 800, 400, 200, 100);

	signal mem_r, mem_rp	: filter_vector := (others => (others => (others => '0')));
	signal r, rp, ra, rb, rf	: volt_vector := (others => (others => '0'));

begin

--	Flags
	flag(0) <= START_BIT;

	flag_end_iteration	: process(iter_number)
	begin
		if iter_number = STAGE_LENGTH-1 then
			flag(1) <= '1';
		else
			flag (1) <= '0';
		end if;
	end process;


--	Finite State Machine
	--	Sequential Logic
	FSM_seq_logic	: process(CLOCK, RESET, ENABLE, fsm_ns)
	begin
		if RESET = '0' then
			fsm_cs <= IDLE;
		elsif rising_edge(CLOCK) and ENABLE = '1' then
			fsm_cs <= fsm_ns;
		end if;
	end process;

	--	Combinational Logic
	FSM_comb_logic	: process(fsm_cs, flag, iter_number)
	begin
		hintx	 <= (others => '0');
		mem_push <= (others => '0');

		case fsm_cs is
			when IDLE	=>
				if flag(0) = '1' then
					fsm_ns <= LOAD;
				else
					fsm_ns <= IDLE;
				end if;

			when LOAD	=>
				hintx(0) <= '1';
				mem_push(iter_number) <= '1';
				fsm_ns <= ITERATE;

			when ITERATE	=>
				hintx(2) <= '1';
				if flag(1) = '1' then
					fsm_ns <= FINAL;
				else
					fsm_ns <= ITERATE;
				end if;
				mem_push(iter_number+1) <= '1';
			when FINAL	=>
				hintx(3) <= '1';
				if flag(0) = '0' then
					fsm_ns <= IDLE;
				else
					fsm_ns <= FINAL;
				end if;

			when others	=>
				fsm_ns <= IDLE;
		end case;
	end process;

--	Iteration number
	process(CLOCK, RESET, ENABLE, hintx, iter_number)
	begin
		if RESET = '0' then
			iter_number <= 0;
		elsif rising_edge(CLOCK) and enable = '1' then
			if hintx(3) = '1' then
				iter_number <= 0;
			elsif hintx(2) = '1' and flag(1) = '0' then
				iter_number <= iter_number + 1;
			end if;
		end if;
	end process;

	--process(mem_r, mem_rp, iter_number)
	--begin
		r <= mem_r(iter_number);
		rp <= mem_rp(iter_number);
		ra <= srr_volt_vect(r, 1);
		rb <= lin_transf_gdsc_red(rp, b, QN_BASE, iter_number);
		rf <= ra+rb;
	--end process;

--	Memory
	mem_alpha0	: gdsc_memory
		generic map	(p(0), DATA_WIDTH)
		port map	(CLOCK, '1', mem_push(0), SIGNAL_ALPHA, mem_r(0)(0), mem_rp(0)(0));
	mem_beta0	: gdsc_memory
		generic map (p(0), DATA_WIDTH)
		port map	(CLOCK, '1', mem_push(0), SIGNAL_BETA, mem_r(0)(1), mem_rp(0)(1));


	buff	: for i in 1 to STAGE_LENGTH-1 generate
		mem_alpha	: gdsc_memory
			generic map	(p(i), DATA_WIDTH)
			port map	(CLOCK, '1', mem_push(i), rf(0), mem_r(i)(0), mem_rp(i)(0));
		mem_beta	: gdsc_memory
			generic map (p(i), DATA_WIDTH)
			port map	(CLOCK, '1', mem_push(i), rf(1), mem_r(i)(1), mem_rp(i)(1));
	end generate;

	process(CLOCK, hintx, rf)
	begin
		if rising_edge(CLOCK) and hintx(3) = '1' then
			FILTERED_ALPHA <= rf(0);
			FILTERED_BETA  <= rf(1);
		end if;
	end process;


--	Output
	END_BIT <= hintx(3);
end architecture beh;
