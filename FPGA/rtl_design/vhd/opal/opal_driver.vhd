library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity opal_driver is
	generic	(
		OPAL_WIDTH_INPUT	: natural;
		OPAL_WIDTH_OUTPUT	: natural
	);
	port 	(
		CLOCK	: in std_logic;
		OPAL_DATAIN	: out std_logic_vector(OPAL_WIDTH_OUTPUT-1 downto 0);
		OPAL_DATAOUT	: in std_logic_vector(OPAL_WIDTH_INPUT-1 downto 0);
		P_IN	: in std_logic_vector(OPAL_WIDTH_OUTPUT-1 downto 0);	--	p0-p15
		P_OUT	: out std_logic_vector(OPAL_WIDTH_INPUT-1 downto 0)		--	p16-p31
	);
end entity opal_driver;

architecture beh of opal_driver is

	signal di	: std_logic_vector(OPAL_WIDTH_INPUT-1 downto 0) := (others => '0');
	signal do	: std_logic_vector(OPAL_WIDTH_OUTPUT-1 downto 0) := (others => '0');

begin

	process(di, P_IN)
	begin
		--	DO Inversion
		for i in 0 to do'length/2-1 loop
			for j in 0 to 1 loop
				do(2*i+j) <= P_IN(2*i+1-j);
			end loop;
		end loop;
		--	DI Inversion
		for i in 0 to di'length/2-1 loop
			for j in 0 to 1 loop
				P_OUT(2*i+j) <= di(2*i+1-j);
			end loop;
		end loop;
	end process;

	process(CLOCK, do, OPAL_DATAOUT)
	begin
		if rising_edge(CLOCK) then
			OPAL_DATAIN <= do;
			di <= OPAL_DATAOUT;
		end if;
	end process;

end architecture beh;
