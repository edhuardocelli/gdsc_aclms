library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library lib_OPAL;
use lib_OPAL.lib_opal.all;

entity opal_spi_protocol is
	generic	(
		CHANNEL_WIDTH : natural;
		DATA_WIDTH	  : natural
--		TIMEOUT_WIDTH : natural
	);
	port	(
		CLOCK	 : in std_logic;
		RESET	 : in std_logic;
		ENABLE	 : in std_logic;
		START	 : in std_logic;
		OPAL_CLK : in std_logic;
		DATA_IN	 : in std_logic_vector(CHANNEL_WIDTH-1 downto 0);
		DATA_OUT : out opal_data_type(CHANNEL_WIDTH-1 downto 0);
		OPAL_CS	 : out std_logic;
		END_ACQ	 : out std_logic
	);
end entity opal_spi_protocol;

architecture beh of opal_spi_protocol is

--	Components
	component edge_detector
		generic (
			POLARITY	: std_logic;
			TIME_DELAY	: natural
		);
		port (
			CLOCK		  : in  std_logic;
			RESET		  : in  std_logic;
			ENABLE		  : in  std_logic;
			INPUT_SIGNAL  : in  std_logic;
			OUTPUT_SIGNAL : out std_logic
		);
	end component edge_detector;

--	Signals
	type fsm_states is (IDLE, EDGE, HOLD, BUSY, FINAL, FAIL);
	signal fsm_cs, fsm_ns	: fsm_states := IDLE;
	signal hintx, flag	: std_logic_vector(3 downto 0) := (others => '0');
	signal fault 	: std_logic := '0';
	signal rx_counter	: unsigned(3 downto 0) := (others => '0');--natural range 0 to DATA_WIDTH;
	signal rx_data	: opal_data_type(DATA_OUT'range) := (others => (others => '0'));
	--	Timeout
--	signal timeout	: std_logic := '0';
--	signal timeout_counter	: unsigned(TIMEOUT_WIDTH-1 downto 0) := (others => '0');

begin

--	Start Communication
	start_capture	: edge_detector
		generic map	('1', 2)
		port map	(CLOCK, RESET, ENABLE, START, flag(0));

--	Finite State Machine
	--	FSM Sequential Logic
	FSM_seq_logic	: process(CLOCK, RESET, ENABLE, fsm_ns)
	begin
		if RESET = '0' then
			fsm_cs <= IDLE;
		elsif rising_edge(CLOCK) and ENABLE = '1' then
			fsm_cs <= fsm_ns;
		end if;
	end process;

	--	FSM Combinational Logic\
	flag(1) <= OPAL_CLK;
	FSM_comb_logic	: process(fsm_cs, flag, rx_counter)
	begin
		hintx	<= (others => '0');
		case fsm_cs is
			when IDLE	=>
				hintx(3) <= '1';
				if flag(0) = '1' then
					fsm_ns <= BUSY;
				else
					fsm_ns <= IDLE;
				end if;
			when BUSY	=>
				hintx(0) <= '1';
				if rx_counter = DATA_WIDTH then
					fsm_ns <= FINAL;
				else
					if flag(1) = '1' then
						fsm_ns <= HOLD;
					else
						fsm_ns <= BUSY;
					end if;
				end if;
			when HOLD	=>
				hintx(0) <= '1';
				if flag(1) = '0' then
					fsm_ns <= EDGE;
				else
					fsm_ns <= HOLD;
				end if;
			when EDGE	=>
				hintx(0) <= '1';
				hintx(2) <= '1';
				fsm_ns <= BUSY;
			when FINAL	=>
				hintx(1) <= '1';
				fsm_ns <= IDLE;
			when others	=>
				fsm_ns <= IDLE;
		end case;
	end process;

	spi_counter	: process(CLOCK, RESET, ENABLE, rx_counter, hintx)
	begin
		if RESET = '0' or hintx(3) = '1' then
			rx_counter <= (others => '0');
		elsif rising_edge(CLOCK) and ENABLE = '1' and hintx(2) = '1'  then
				rx_counter <= rx_counter + 1;
		end if;
	end process;

	data_acquisition	: process(CLOCK, RESET, ENABLE, DATA_IN, hintx, rx_data)
	begin
		if RESET = '0' then
			rx_data <= (others => (others => '0'));
		elsif rising_edge(CLOCK) and ENABLE = '1' then
			if hintx(2) = '1' then
				for i in rx_data'range loop
					rx_data(i)(rx_data(i)'length-1 downto 1) <= rx_data(i)(rx_data(i)'length-2 downto 0);
					rx_data(i)(0) <= DATA_IN(i);
				end loop;
			end if;
		end if;
	end process;

	OPAL_CS <= hintx(0);
	END_ACQ <= hintx(1);

	process(CLOCK, RESET, ENABLE, hintx, rx_data)
	begin
		if RESET = '0' then
			DATA_OUT <= (others => (others => '0'));
		elsif rising_edge(CLOCK) and ENABLE = '1' then
			if hintx(1) = '1' then
				DATA_OUT <= rx_data;
			end if;
		end if;
	end process;

end architecture beh;
