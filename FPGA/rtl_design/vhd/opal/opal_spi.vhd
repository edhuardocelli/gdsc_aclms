library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library opal;
use opal.lib_opal.all;

entity opal_spi_protocol is
	generic	(
		CHANNEL_WIDTH : natural;
		DATA_WIDTH	  : natural
	);
	port	(
		CLOCK	 : in std_logic;
		RESET	 : in std_logic;
		ENABLE	 : in std_logic;
		START	 : in std_logic;
		OPAL_CLK : in std_logic;
		DATA_IN	 : in std_logic_vector(CHANNEL_WIDTH-1 downto 0);
		DATA_OUT : out opal_data_type(CHANNEL_WIDTH-1 downto 0);
		OPAL_CS	 : out std_logic;
		END_ACQ	 : out std_logic
	);
end entity opal_spi_protocol;

architecture beh of opal_spi_protocol is

--	Components
	component edge_detector
		generic (
			POLARITY	: std_logic;
			TIME_DELAY	: natural
		);
		port (
			CLOCK		  : in  std_logic;
			RESET		  : in  std_logic;
			ENABLE		  : in  std_logic;
			INPUT_SIGNAL  : in  std_logic;
			OUTPUT_SIGNAL : out std_logic
		);
	end component edge_detector;

--	Signals
	type fsm_states is (IDLE, EDGE, HOLD, BUSY, FINAL, FAIL);
	signal fsm_cs, fsm_ns	: fsm_states := IDLE;
	signal hintx, flag	: std_logic_vector(5 downto 0) := (others => '0');
	signal fault 	: std_logic := '0';
	--	SPI Counter
	signal rx_counter	: unsigned(3 downto 0) := (others => '0');--natural range 0 to DATA_WIDTH;
	signal rx_data	: opal_data_type(DATA_OUT'range) := (others => (others => '0'));
	--	SPI Chip Select
	signal spi_cs	: std_logic := '0';
	--	Timeout
	constant TIMEOUT_WIDTH	: natural := 13;
	signal timeout	: std_logic := '0';
	signal timeout_counter	: unsigned(TIMEOUT_WIDTH-1 downto 0) := (others => '0');

	signal timeout_debug	: unsigned(5 downto 0) := (others => '0');

begin

--	Flags
	--	Communication
	start_capture	: edge_detector
		generic map	('1', 2)
		port map	(CLOCK, RESET, ENABLE, START, flag(0));
	--	Clock (from OPAL-RT)
	flag(1) <= OPAL_CLK;

--	Finite State Machine
	--	FSM Sequential Logic
	FSM_seq_logic	: process(CLOCK, RESET, ENABLE, fsm_ns, timeout)
	begin
		if RESET = '0' then
			fsm_cs <= IDLE;
		elsif rising_edge(CLOCK) and ENABLE = '1' then
			if timeout = '1' then
				fsm_cs <= FAIL;
			else
				fsm_cs <= fsm_ns;
			end if;
		end if;
	end process;

	--	FSM Combinational Logic\
	FSM_comb_logic	: process(fsm_cs, flag, rx_counter)
	begin
		hintx	<= (others => '0');
		case fsm_cs is
			when IDLE	=>
				hintx(0) <= '1';
				if flag(0) = '1' then
					fsm_ns <= BUSY;
				else
					fsm_ns <= IDLE;
				end if;
			when BUSY	=>
				hintx(1) <= '1';
				if flag(2) = '1' then
					fsm_ns <= FINAL;
				else
					if flag(1) = '1' then
						fsm_ns <= HOLD;
					else
						fsm_ns <= BUSY;
					end if;
				end if;
			when HOLD	=>
				hintx(2) <= '1';
				if flag(1) = '0' then
					fsm_ns <= EDGE;
				else
					fsm_ns <= HOLD;
				end if;
			when EDGE	=>
				hintx(3) <= '1';
				fsm_ns <= BUSY;
			when FINAL	=>
				hintx(4) <= '1';
				fsm_ns <= IDLE;
			when FAIL	=>
				hintx(5) <= '1';
				fsm_ns <= IDLE;
			when others	=>
				fsm_ns <= IDLE;
		end case;
	end process;

	spi_cs <= hintx(1) or hintx(2) or hintx(3);

	timeout_proc	: process(CLOCK, RESET, ENABLE, timeout_counter, spi_cs)
	begin
		if RESET = '0' or spi_cs = '0' then
			timeout_counter <= (others => '0');
			timeout <= '0';
		elsif rising_edge(CLOCK) and ENABLE = '1' then
			if timeout_counter = 8191 then
				timeout <= '1';
				timeout_counter <= (others => '0');
			else
				timeout_counter <= timeout_counter + 1;
				timeout <= '0';
			end if;
		end if;
	end process;


	spi_counter	: process(CLOCK, RESET, ENABLE, rx_counter, hintx)
	begin
		if RESET = '0' or (hintx(0) or hintx(4)) = '1' then
			rx_counter <= (others => '0');
		elsif rising_edge(CLOCK) and ENABLE = '1' and hintx(3) = '1'  then
				rx_counter <= rx_counter + 1;
		end if;
	end process;

	process(CLOCK, rx_counter)
	begin
		if rising_edge(CLOCK) then
			if rx_counter = DATA_WIDTH then
				flag(2) <= '1';
			else
				flag(2) <= '0';
			end if;
		end if;
	end process;

	data_acquisition	: process(CLOCK, RESET, ENABLE, DATA_IN, hintx, rx_data)
	begin
		if RESET = '0' then
			rx_data <= (others => (others => '0'));
		elsif rising_edge(CLOCK) and ENABLE = '1' then
			if hintx(3) = '1' then
				for i in rx_data'range loop
					rx_data(i)(rx_data(i)'length-1 downto 1) <= rx_data(i)(rx_data(i)'length-2 downto 0);
					rx_data(i)(0) <= DATA_IN(i);
				end loop;
			end if;
		end if;
	end process;

	process(CLOCK, RESET, ENABLE, hintx, rx_data)
	begin
		if RESET = '0' then
			DATA_OUT <= (others => (others => '0'));
		elsif rising_edge(CLOCK) and ENABLE = '1' and hintx(4) = '1' then
			DATA_OUT <= rx_data;
		end if;
	end process;

	OPAL_CS <= spi_cs;
	END_ACQ <= hintx(4);

end architecture beh;
