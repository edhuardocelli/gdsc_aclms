library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sympwm is
	generic	(
		RES	: natural;
		MAX_COUNTER	: natural;
		INT_GEN	: natural
	);
	port	(
		clock, enable	: in std_logic;
		ref_A, ref_B	: in unsigned(RES-1 downto 0);
		pulse_A, pulse_B, intx 	: out std_logic
	);
end entity sympwm;

architecture pwm_beh of sympwm is

	component sympulse is
		generic	(
			RES	: natural := RES;
			MAX_COUNTER	: natural	:= MAX_COUNTER
		);
		port	(
			clock, enable	: in std_logic;
			r, cnt	: in unsigned(RES-1 downto 0);
			pulse	: out std_logic
		);
	end component sympulse;

	signal irq	: std_logic := '0';
	signal rA, rB	: unsigned(RES-1 downto 0) := (others => '0');
	signal cnt	: unsigned(RES-1 downto 0) := (others => '0');

begin

in_reg	: process(ref_A, ref_B, irq)
begin
	if rising_edge(irq) then
		rA <= ref_A;
		rB <= ref_B;
	end if;
end process;

counter	: process(clock, cnt)
begin
	if rising_edge(clock) then
		if cnt = MAX_COUNTER-1 then
			cnt <= (others => '0');
		else
			cnt <= cnt + 1;
		end if;
	end if;
end process;

pulsea	: sympulse port map (clock, enable, rA, cnt, pulse_A);
pulseb	: sympulse port map (clock, enable, rB, cnt, pulse_B);

interrupt	: process(cnt, clock, irq)
begin
	if INT_GEN = 1 then
		if cnt = 0 or cnt = MAX_COUNTER/2 then
			irq <= '1';
		elsif rising_edge(clock) then
			if cnt = MAX_COUNTER/4 or cnt = 3*MAX_COUNTER/4 then
				irq <= '0';
			end if;
		end if;
		intx <= irq;
	else
		intx <= '0';
	end if;
end process;

end architecture pwm_beh;
