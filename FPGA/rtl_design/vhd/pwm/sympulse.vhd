library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sympulse is
	generic	(
		RES	: natural;
		MAX_COUNTER	: natural
	);
	port	(
		clock, enable	: in std_logic;
		r, cnt	: in unsigned(RES-1 downto 0);
		pulse	: out std_logic
	);
end entity sympulse;

architecture beh of sympulse is

begin

pwm_pulse	: process(clock, enable, r, cnt)
	variable p 	: std_logic_vector(1 downto 0) := "00";
begin
	if r > cnt then
		p(0) := '1';
	else
		p(0) := '0';
	end if;

	if MAX_COUNTER-r <= cnt then
		p(1) := '1';
	else
		p(1) := '0';
	end if;

	if enable = '0' then
		pulse <= '0';
	elsif rising_edge(clock) then
		pulse <= p(1) or p(0);
	end if;
end process;

end architecture beh;
