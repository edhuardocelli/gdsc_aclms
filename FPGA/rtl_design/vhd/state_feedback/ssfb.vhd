library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ssfb is
	generic	(
		RES	: natural;
		QRES	: natural;
		QRESw	: natural
	);
	port	(
		clock, enable	: in std_logic;
		r, fb	: in signed(RES-1 downto 0);
		x1, x2, x3	: in signed(RES-1 downto 0);
		y 	: out signed(RES-1 downto 0);
		done	: out std_logic
	);
end entity ssfb;

architecture ssfb_beh of ssfb is

	function smult_qn	(
		u, v 	: signed;
		Qn	: natural
	)	return signed is
	begin
		return resize(shift_right(u*v, Qn), u'length);
	end function smult_qn;

	component ressys is
		generic	(
			RES	: natural;
			QRES : natural;
			WR_RES	: natural;
			CSI_RES	: natural;
			GAIN1	: natural;
			GAIN2	: natural
		);
		port	(
			clock, enable	: in std_logic;
			u 	: in signed(RES-1 downto 0);
			y	: out signed(RES-1 downto 0)
		);
	end component ressys;

	signal u 	: signed(RES-1 downto 0) := (others => '0');
	signal kr, kx	: signed(RES-1 downto 0) := (others => '0');
	--type gain is array(1 to 3) of signed(RES-1 downto 0);
	--constant stgain	: gain := (to_signed(4422, RES), to_signed(1342, RES), to_signed(3721, RES));
	type gain is array(1 to 4) of signed(RES-1 downto 0);
	constant stgain	: gain := (to_signed(11683, RES), to_signed(-980, RES), to_signed(1730, RES), to_signed(-175, RES));

	--	Finite State Machine Signals
	type fsm_states is (st_hold, st_res, st_gain, st_end);
	signal fsm_cs, fsm_ns	: fsm_states := st_hold;
	signal intx	: std_logic_vector(2 downto 0) := (others => '0');
	signal up	: signed(RES-1 downto 0) := (others => '0');
begin

--	Finite State Machine
	--	Sequential
	fsm_seq	: process(clock, fsm_ns)
	begin
		if rising_edge(clock) then
			fsm_cs <= fsm_ns;
		end if;
	end process;

	--	Combinational
	fsm_comb	: process(fsm_cs, enable)
	begin
		intx <= (others => '0');
		case fsm_cs is
			when st_hold	=>		--	Wait
				if enable = '1' then
					fsm_ns <= st_res;
				else
					fsm_ns <= st_hold;
				end if;

			when st_res	=>		--	Resonant Controller
				intx(0)	<= '1';
				fsm_ns	<= st_gain;

			when st_gain	=>		--	State feedback
				intx(1)	<= '1';
				fsm_ns	<= st_end;

			when st_end	=>		--	End
				intx(2)	<= '1';
				fsm_ns	<= st_hold;

			when others	=>		--	Glitch Free
				fsm_ns	<= st_hold;

		end case;
	end process;



--	Feedback
	u <= r-fb;

--	State Gains
res_1	: ressys
	generic map (27, 18, 2059, 262136, 2046129, 201195)
	port map (clock, intx(0), u, kr);

feedback	: process(clock, intx, x1, x2, x3)
begin
	if rising_edge(clock) and intx(1) = '1' then
		kx <= smult_qn(x1,stgain(1),QRES) + smult_qn(x2,stgain(2),QRES) + smult_qn(x3,stgain(3),QRES) + smult_qn(up,stgain(4),QRES);
	end if;
end process;

ctrl_output	: process(clock, intx, kr, kx, u)
	begin
		if rising_edge(clock) and intx(2) = '1' then
			y <=	kr - kx;
			up <= u;
		end if;
	end process;

--	End Flag
	done <= intx(2);

end architecture ssfb_beh;
