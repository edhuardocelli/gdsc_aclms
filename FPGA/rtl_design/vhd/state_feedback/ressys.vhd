library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ressys is
	generic	(
		RES	: natural;
		QRES : natural;
		WR_RES	: natural;
		CSI_RES	: natural;
		GAIN1	: natural;
		GAIN2	: natural
	);
	port	(
		clock, enable	: in std_logic;
		u 	: in signed(RES-1 downto 0);
		y	: out signed(RES-1 downto 0)
	);
end entity ressys;

architecture sysbeh of ressys is

	function smult_qn	(
		u, v 	: signed;
		Qn	: natural
	)	return signed is
	begin
		return resize(shift_right(u*v, Qn), u'length);
	end function smult_qn;

	type resonant_state is array(1 to 2) of signed(RES-1 downto 0);
--	signal u 	: signed(RES-1 downto 0) := (others => '0');
	signal xres	: resonant_state := (others => (others =>'0'));
	constant wr	: signed(RES-1 downto 0) := to_signed(WR_RES, RES);
	constant csi	: signed(RES-1 downto 0) := to_signed(CSI_RES, RES);
	constant gain	: resonant_state := (to_signed(GAIN1, RES), to_signed(GAIN2, RES));
begin

res_sys	: process(clock, enable, xres)
	begin
		if rising_edge(clock) and enable = '1' then
			xres(1) <= smult_qn(csi,xres(1),QRES) + smult_qn(wr,xres(2),QRES);-- + shift_right(u,15);
			xres(2) <= smult_qn(csi,xres(2),QRES) - smult_qn(wr,xres(1),QRES) + smult_qn(wr,u,QRES);
		end if;
	end process;

	y <= smult_qn(gain(1), xres(1), QRES) + smult_qn(gain(2), xres(2), QRES);

end architecture sysbeh;
