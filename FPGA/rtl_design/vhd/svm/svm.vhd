library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity svm is
	generic	(
		DATA_WIDTH	: natural;
		CLOCK_FREQUENCY	: real;
		SAMPL_FREQUENCY	: real;
		QN_BASE	: natural
	);
	port	(
		CLOCK	  : in std_logic;
		RESET	  : in std_logic;
		ENABLE	  : in std_logic;
		START_BIT : in std_logic;
		SAMPLE_BIT: in std_logic;
		ALPHA	  : in signed(DATA_WIDTH-1 downto 0);
		BETA	  : in signed(DATA_WIDTH-1 downto 0);
		SWITCHES  : out std_logic_vector(2 downto 0);
		INTX	  : out std_logic;
		END_SVM	  : out std_logic
	);
end entity svm;

architecture beh of svm is

--	Components
	component edge_detector
		generic (
			POLARITY	: std_logic;
			TIME_DELAY	: natural
		);
		port (
			CLOCK		  : in  std_logic;
			RESET		  : in  std_logic;
			ENABLE		  : in  std_logic;
			INPUT_SIGNAL  : in  std_logic;
			OUTPUT_SIGNAL : out std_logic
		);
	end component edge_detector;

	component sv_modulator
		generic	(
			CARRIER_WIDTH : natural;
			CARRIER_MAX	  : natural
		);
		port 	(
			CLOCK 		: in std_logic;
			RESET		: in std_logic;
			ENABLE 		: in std_logic;
			TIME_G		: in unsigned(CARRIER_WIDTH-1 downto 0);
			TIME_H		: in unsigned(CARRIER_WIDTH-1 downto 0);
			SWAP_TIMES	: in std_logic;
			PWM_STATE	: out natural range 0 to 3;
			INTX		: out std_logic
		);
	end component sv_modulator;

	component moving_avg_pwm
	generic (
	  DATA_WIDTH   : natural;
	  MEMORY_WIDTH : natural;
	  SCALE_FACTOR : natural
	);
	port (
	  CLOCK        : in  std_logic;
	  RESET        : in  std_logic;
	  ENABLE       : in  std_logic;
	  INPUT_SIGNAL : in  std_logic;
	  AVERAGE      : out signed(DATA_WIDTH-1 downto 0)
	);
	end component moving_avg_pwm;


--	Important Constants
	constant carrier_max	: natural := natural(CLOCK_FREQUENCY/SAMPL_FREQUENCY);
	constant carrier_width	: natural := natural(ceil(log(real(carrier_max),2.0)));

--	Type Declarations
	type fsm_states is (IDLE, LOAD, TIMES, TIMESWAP, FINAL);

	type transf_mx_real is array(0 to 1, 0 to 1) of real;
	type transf_mx_signed is array(0 to 1, 0 to 1) of signed(ALPHA'range);
	type base_gh is array(0 to 1) of signed(ALPHA'range);

	type possible_duty_cycles is array(0 to 2) of signed(ALPHA'range);
	type modulator_time is array(0 to 1) of signed(ALPHA'range);
	type modulator_time_uns is array(0 to 1) of unsigned(carrier_width-1 downto 0);

	type swtable is array(0 to 5, 0 to 3) of integer range 0 to 7;
	type vtable is array(0 to 7) of std_logic_vector(SWITCHES'range);

--	Function Declarations
	--	QN Multiplication
	function smult_qn	(
		u, v 	: signed;
		QN_BASE	: natural
	)	return signed is
	begin
		return resize(shift_right(u*v, QN_BASE), u'length);
	end function smult_qn;

	function umult_qn	(
		k	: integer;
		x	: signed;
		QN_BASE	: natural;
		OUTPUT_WIDTH	: natural
	)	return unsigned is
	begin
		return resize(shift_right(to_unsigned(k, QN_BASE)*resize(unsigned(x), QN_BASE), QN_BASE), OUTPUT_WIDTH);
	end function umult_qn;

	--	Sign Function
	function sign	(
		s 	: signed
	)	return std_logic is
	begin
		return s(s'length-1);
	end function sign;

	--	MX real to SIGNED type conversion
	function mx_real_to_signed	(
		M_real			: transf_mx_real;
		QN_BASE			: natural
	)	return transf_mx_signed is
		variable M_signed	: transf_mx_signed;
	begin
		for i in M_real'reverse_range loop
			for j in M_real'reverse_range loop
				M_signed(i,j) := to_signed(integer(2.0**(QN_BASE)*M_real(i,j)), M_signed(i,j)'length);
			end loop;
		end loop;
		return M_signed;
	end function mx_real_to_signed;

	--	Linear Transformation of a Vector
	function lin_transf	(
		T	: transf_mx_signed;
		x	: base_gh;
		QN_BASE	: natural
	)	return base_gh is
		variable sum	: base_gh;
	begin
		for i in x'reverse_range loop
			sum(i) := (others => '0');
			for j in x'reverse_range loop
				sum(i) := sum(i) + smult_qn(T(i,j),x(j), QN_BASE);
			end loop;
		end loop;
		return sum;
	end function lin_transf;

--	Signal Declarations
	--	FSM Signals
	signal cs, ns	: fsm_states := IDLE;
	signal irq	: std_logic_vector(3 downto 0) := (others => '0');
	signal start	: std_logic := '0';
	--	GH Transformation Matrix
	constant Tgh_real : transf_mx_real := ((1.5, 0.0-0.5*sqrt(3.0)), (0.0, sqrt(3.0)));
	constant Tgh	: transf_mx_signed := mx_real_to_signed(Tgh_real, QN_BASE);

	--	Modulation Index
	signal m, mgh	: base_gh := (others => (others => '0'));

	--	Sextant signals
	signal d	: possible_duty_cycles := (others => (others => '0'));
	signal sext_ctrl_signal	: std_logic_vector(2 downto 0) := (others => '0');
	signal sextant, sextant_p	: natural range 0 to 5 := 1;
	signal sector	: natural range 0 to 11 := 1;

	--	Time calculation signals
	signal t	: modulator_time	:= (others => (others => '0'));
	signal ts, tp	: modulator_time_uns	:= (others => (others => '0'));
	signal swap_times, swap_times_p	: std_logic := '0';

	--	SVM PWM Modulation Signals
	type pwm_times is array(0 to 2) of signed(ALPHA'range);
	signal p 	: pwm_times := (others => (others => '0'));

	--	SVM Tables
	constant switching_table	: swtable := ((0,1,2,7),(0,3,2,7),(0,3,4,7),(0,5,4,7),(0,5,6,7),(0,1,6,7));
	constant vector_table	: vtable	:= ("000","100","110","010","011","001","101","111");
--	signal debug	: natural := 0;
	signal modulator_state : natural range 0 to 3 := 0;

	--	Interrupt Bit
	signal intx_sample	: std_logic := '0';

	signal sw	: std_logic_vector(SWITCHES'range) := (others => '0');


begin

--	Start Bit
start_capture	: edge_detector
	generic map	('1', 2)
	port map 	(CLOCK, RESET, ENABLE, START_BIT, start);


--	Finite State Machine
	FSM_seq_logic	: process(CLOCK, ns)
	begin
		if rising_edge(CLOCK) then
			cs <= ns;
		end if;
	end process;

	FSM_comb_logic	: process(CLOCK, cs, start)
	begin
		irq <= (others => '0');
		case cs is
			when IDLE 	=>
				if start = '1' then
					ns <= LOAD;
				else
					ns <= IDLE;
				end if;

			when LOAD	=>
				irq(0) <= '1';
			 	ns <= TIMES;

			when TIMES	=>
				irq(1) <= '1';
				ns <= TIMESWAP;

			when TIMESWAP	=>
				irq(2) <= '1';
				ns <= FINAL;

			when FINAL	=>
				irq(3) <= '1';
				ns <= IDLE;

			when others	=>
				ns <= IDLE;
		end case;
	end process;

--	GH Components
	m <= (ALPHA, BETA);

	gh_transformation	: process(CLOCK, RESET, m, irq)
	begin
		if RESET = '0' then
			mgh <= (others => (others => '0'));
		else
			if rising_edge(CLOCK) and irq(0) = '1' then
				mgh <= lin_transf(Tgh, m, QN_BASE);
			end if;
		end if;
	end process;

--	Sector Detection

	--	Possible Duty Cycles
	d(0) <= mgh(0);
	d(1) <= mgh(1);
	d(2) <= mgh(0)+mgh(1);

	--	Sextant Control Signal
	sext_ctrl_signal <= (sign(d(0)), sign(d(1)), sign(d(2)));

	sextant_ctrl	: process(sext_ctrl_signal)
	begin
		case sext_ctrl_signal is
			when "000"	=>
				sextant <= 0;
			when "001"	=>
				sextant <= 0;
			when "010"	=>
				sextant <= 5;
			when "011"	=>
				sextant <= 4;
			when "100"	=>
				sextant <= 1;
			when "101"	=>
				sextant <= 2;
			when "110"	=>
				sextant <= 3;
			when "111"	=>
				sextant <= 3;
			when others	=>
				sextant <= 0;
		end case;
	end process;

--	Modulation Time Calculation

	--	Modulation Times
	svm_times	: process(CLOCK, irq, sextant, d)
	begin
		if rising_edge(CLOCK) and irq(1) = '1' then
			case sextant is
				when 0	=>
					t(0) <= d(0);
					t(1) <= d(1);
				when 1	=>
					t(0) <= -d(0);
					t(1) <= d(2);
				when 2	=>
					t(0) <= d(1);
					t(1) <= -d(2);
				when 3	=>
					t(0) <= -d(1);
					t(1) <= -d(0);
				when 4	=>
					t(0) <= -d(2);
					t(1) <= d(0);
				when 5	=>
					t(0) <= d(2);
					t(1) <= -d(1);
				when others	=>
					t(0) <= (others => '0');
					t(1) <= (others => '0');
			end case;
		end if;
	end process;

	--	Quarter Wave Symmetry
	svm_quarter_wave	: process(CLOCK, irq, t)
		variable tx	: modulator_time_uns := (others => (others => '0'));
	begin
		for i in t'range loop
			tx(i) := umult_qn(carrier_max, t(i), QN_BASE, carrier_width);
		end loop;
		if rising_edge(CLOCK) and irq(2) = '1' then
			if tx(0) > tx(1) then
				swap_times <= '0';
				ts <= tx;
			else
				swap_times <= '0';
				ts(0) <= tx(0);
				ts(1) <= tx(1);
			end if;
		end if;
	end process;

	pwm_update	: process(CLOCK, SAMPLE_BIT, ts, sextant, swap_times)
	begin
		if rising_edge(CLOCK) and SAMPLE_BIT = '1' then
			tp <= ts;
			sextant_p <= sextant;
			swap_times_p <= swap_times;
		end if;
	end process;

	pwm_map	: sv_modulator
		generic map (carrier_width, carrier_max)
		port map	(CLOCK, RESET, ENABLE, tp(0), tp(1), swap_times_p, modulator_state, intx_sample);

--	debug <= switching_table(sextant,modulator_state);
	sw <= vector_table(switching_table(sextant_p,modulator_state));

--	Output
	SWITCHES <= sw;
	INTX 	 <= intx_sample;
	END_SVM	 <= irq(3);

--	Debug
--	avgx : for i in 0 to 2 generate
--		avg_i	: moving_avg_pwm
--			generic map	(DATA_WIDTH, 2*carrier_max, 0)
--			port map 	(CLOCK, '1', '1', sw(i),open);
--		end generate avgx;

end architecture beh;
