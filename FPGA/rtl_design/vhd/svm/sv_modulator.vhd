library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity sv_modulator is
	generic	(
		CARRIER_WIDTH : natural;
		CARRIER_MAX	  : natural
	);
	port 	(
		CLOCK 		: in std_logic;
		RESET		: in std_logic;
		ENABLE 		: in std_logic;
		TIME_G		: in unsigned(CARRIER_WIDTH-1 downto 0);
		TIME_H		: in unsigned(CARRIER_WIDTH-1 downto 0);
		SWAP_TIMES	: in std_logic;
		PWM_STATE	: out natural range 0 to 3;
		INTX		: out std_logic
	);
end entity sv_modulator;

architecture beh of sv_modulator is

	type fsm_states is (S0, S1, S2, S3, FAIL);
	type duty_cycle_vector is array(0 to 2) of unsigned(TIME_G'range);

	signal cs, ns	: fsm_states := S0;

	signal d	: duty_cycle_vector := (others => (others => '0'));

	signal carrier	: unsigned(TIME_G'range) := (others => '0');
	signal carrier_toggle, sample_command, carrier_toggle_mem	: std_logic := '0';
	signal pwm_pulses	: std_logic_vector(2 downto 0);

	signal mod_state	: natural range 0 to 3;


begin

--	Sampler
	process(CLOCK, TIME_H, TIME_G, carrier_toggle, carrier_toggle_mem, sample_command)
		--variable carrier_toggle_mem	: std_logic := '0';
		variable t	: duty_cycle_vector := (others => (others => '0'));
	begin
		if carrier_toggle = '0' then
			t(0) := shift_right(CARRIER_MAX-TIME_G-TIME_H, 1);
			t(1) := t(0) + TIME_G;
			t(2) := t(1) + TIME_H;
		else
			t(0) := shift_right(CARRIER_MAX-TIME_G-TIME_H, 1);
			t(1) := t(0) + TIME_H;
			t(2) := t(1) + TIME_G;
		end if;
		if rising_edge(CLOCK) then
			carrier_toggle_mem <= carrier_toggle;
			if (carrier_toggle xor carrier_toggle_mem) = '1' then
				sample_command <= '1';
				d <= t;
			else
				sample_command <= '0';
			end if;
		end if;
	end process;
--	PWM Carrier Generation
	pwm_carrier	: process(CLOCK, RESET, ENABLE, carrier, carrier_toggle)
	begin
		if RESET = '0' then
			carrier <= (others => '0');
			carrier_toggle <= '0';
		else
			if ENABLE = '1' then
				if rising_edge(CLOCK) then
					if carrier = carrier_max-1 then
						carrier_toggle <= not(carrier_toggle);
						carrier <= (others => '0');
					else
						carrier <= carrier + 1;
					end if;
				end if;
			end if;
		end if;
	end process;

--	pwm_comparison
	pwm_comparison	: process(CLOCK,d, carrier)
	begin
		if rising_edge(CLOCK) then
			for i in d'range loop
				if carrier < d(i) then
					pwm_pulses(i) <= '1';
				else
					pwm_pulses(i) <= '0';
				end if;
			end loop;
		end if;
	end process;

--	Comparison
	pwm_state_seq	: process(CLOCK, RESET, ENABLE, ns)
	begin
		if RESET = '0' then
			cs <= S0;
		elsif rising_edge(CLOCK) and ENABLE = '1' then
			cs <= ns;
		end if;
	end process;

	pwm_state_comb	: process(SWAP_TIMES, cs, pwm_pulses, carrier_toggle)
	begin
		mod_state <= 0;
		case cs is
			when S0	=>
				if SWAP_TIMES = '0' then
					mod_state <= 0;
				else
					mod_state <= 3;
				end if;
				if pwm_pulses(1) = '1' and pwm_pulses(0) = '0' then
					ns <= S1;
				else
					ns <= S0;
				end if;
			when S1	=>
				if SWAP_TIMES = '0' then
					mod_state <= 1;
				else
					mod_state <= 2;
				end if;
				if carrier_toggle = '0' then
					if pwm_pulses(1) = '0' then
						ns <= S2;
					else
						ns <= S1;
					end if;
				else
					if pwm_pulses(2) = '0' then
						ns <= S0;
					else
						ns <= S1;
					end if;
				end if;
			when S2	=>
				if SWAP_TIMES = '0' then
					mod_state <= 2;
				else
					mod_state <= 1;
				end if;
				if carrier_toggle = '0' then
					if pwm_pulses(2) = '0' then
						ns <= S3;
					else
						ns <= S2;
					end if;
				else
					if pwm_pulses(1) = '0' then
						ns <= S1;
					else
						ns <= S2;
					end if;
				end if;
			when S3	=>
				if SWAP_TIMES = '0' then
					mod_state <= 3;
				else
					mod_state <= 0;
				end if;
				if pwm_pulses(1) = '1' and pwm_pulses(0) = '0' then
					ns <= S2;
				else
					ns <= S3;
				end if;
			when others	=>
				ns <= S0;
		end case;
	end process;

	--	OUTPUT
	PWM_STATE <= mod_state;
	INTX	  <= carrier_toggle;

end architecture beh;
