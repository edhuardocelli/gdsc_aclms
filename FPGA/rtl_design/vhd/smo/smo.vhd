library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity smo is
	generic	(
		RES	: natural;
		QRES	: natural
	);
	port 	(
		clock, enable	: in std_logic;
		v_f, v_l	: in signed(RES-1 downto 0);
		v_c, i_l	: in signed(RES-1 downto 0);
		i_f	: out signed(RES-1 downto 0);
		done	: out std_logic
	);
end entity smo;

architecture smo_beh of smo is

	type matrix is array(1 to 3, 1 to 3) of integer range -134217728 to 134217727;
	type signed_mx is array(1 to 3, 1 to 3) of signed(RES-1 downto 0);
	type signed_vec is array(1 to 3) of signed(RES-1 downto 0);
	type signed_out is array(1 to 3) of signed(RES-1 downto 0);


	function eval_At return signed_mx is
		variable At	: signed_mx;
		constant A	: matrix := ((34295, 19723, 17819), (-139457, 216866, 307006), (17819, -43419, 229357));
	begin
		for i in 1 to 3 loop
			for j in 1 to 3 loop
				At(i,j) := to_signed(A(i,j), RES);
			end loop;
		end loop;
		return At;
	end function eval_At;
	constant At	: signed_mx	:= eval_At;
	constant Bt	: signed_vec	:= (to_signed(1273, RES), to_signed(28382, RES), to_signed(44693, RES));	-- Euler



	function smult_qn	(
		u, v 	: signed;
		Qn	: natural
	)	return signed is
	begin
		return resize(shift_right(u*v, Qn), u'length);
	end function smult_qn;

	function mult_mxv	(
		A	: signed_mx;
		v	: signed_vec
	)	return signed_vec is
		variable y	: signed_vec;
	begin
		for i in 1 to 3 loop
			y(i) := smult_qn(A(i,1),v(1),QRES) + smult_qn(A(i,2),v(2),QRES) + smult_qn(A(i,3),v(3),QRES);
		end loop;
		return y;
	end function mult_mxv;

	function mult_gainvec	(
		v	: signed_vec;
		K	: signed
	)	return signed_vec is
		variable y	: signed_vec;
	begin
		for i in 1 to 3 loop
			y(i) := smult_qn(v(i), k, QRES);
		end loop;
		return y;
	end function mult_gainvec;

	function sum_vec	(
		u, v	: signed_vec
	)	return signed_vec is
		variable w	: signed_vec;
	begin
		for i in 1 to 3 loop
			w(i) := u(i) + v(i);
		end loop;
		return w;
	end function sum_vec;

--	constant Bt	: signed_vec	:= (to_signed(3182, RES), to_signed(54823, RES), to_signed(40957, RES));	-- Impulse
	signal x	: signed_vec	:= (others => (others => '0'));
	signal u, w	: signed(RES-1 downto 0) := (others => '0');

	--	Finite State Machine
	type fsm_states is (st_hold, st_error, st_updt, st_end);
	signal fsm_cs, fsm_ns	: fsm_states := st_hold;
	signal intx	: std_logic_vector(2 downto 0) := (others => '0');

	--	Estimator Gains
	type gain_vec is array(1 to 3) of integer range -134217728 to 134217727;
	constant L1	: gain_vec := (262144, 262144, 262144);
	constant L2	: gain_vec := (262144/10, 262144/10, 262144/10);--(262144/100, 262144/100);
	--	Estimator Signals
	signal y, ye, e, v	: signed_out	:= (others => (others => '0'));
	signal xe	: signed(RES-1 downto 0)	:= (others => '0');
	--	Expanded System
	signal csi	: signed(RES-1 downto 0)	:= (others => '0');

	--	Debug
	signal ex	: signed(RES-1 downto 0) := (others => '0');

begin

	--	Finite State Machine
		--	Sequential
	fsm_seq	: process(clock, fsm_ns)
	begin
		if rising_edge(clock) then
			fsm_cs <= fsm_ns;
		end if;
	end process;

		--	Combinational
	fsm_comb	: process(fsm_cs, enable)
	begin
		intx <= (others => '0');
		case fsm_cs is
			when st_hold	=>
				if enable = '1' then
					fsm_ns <= st_error;
				else
					fsm_ns <= st_hold;
				end if;

			when st_error	=>
				intx(0) <= '1';
				fsm_ns <= st_updt;

			when st_updt	=>
				intx(1) <= '1';
				fsm_ns <= st_end;

			when st_end	=>
				intx(2) <= '1';
				fsm_ns <= st_hold;

			when others	=>
				fsm_ns <= st_hold;
		end case;
	end process;

	--	Input and Measurements
	u <= v_f;
	y <= (i_l, v_c, csi);

	process(clock, intx, u, v_f)
	begin
		if rising_edge(clock) and intx(2) = '1' then
			csi <= u;
		end if;
	end process;

	--	Sliding Mode Control Action
	process(clock, intx, e, y, ye)
	begin
		for i in 1 to 3 loop
			e(i) <= y(i) - ye(i);

			if rising_edge(clock) and intx(0) = '1' then
				if e(i) = 0 then
					v(i) <= (others => '0');
				else
					if e(i)(e(i)'length-1) = '0' then
						v(i) <= to_signed(L1(i), RES);
					else
						v(i) <= to_signed(-L1(i), RES);
					end if;
				end if;
			end if;
		end loop;
	end process;

	--	System Update Equations
	process(clock, intx, y, ye, v, u, xe)
	begin
		if rising_edge(clock) and intx(1) = '1' then
			--	Measurement Estimate
			for i in 1 to 2 loop
				ye(i) <= smult_qn(At(i,1), ye(1), QRES) + smult_qn(At(i,2), ye(2), QRES) + smult_qn(At(i,3), xe, QRES) + smult_qn(Bt(i), ye(3), QRES) + v(i);
			end loop;
			ye(3) <= u + v(3);
			--	State Estimate
			xe <= smult_qn(At(3,1), y(1), QRES) + smult_qn(At(3,2), y(2), QRES) + smult_qn(At(3,3), xe, QRES) + smult_qn(Bt(3), y(3), QRES)
				- smult_qn(to_signed(L2(1), RES), v(1), QRES) - smult_qn(to_signed(L2(2), RES), v(2), QRES) - smult_qn(to_signed(L2(3), RES), v(3), QRES);
		end if;
	end process;
	i_f <= xe;
	--	Output
	--i_f <= xe;
	done <= intx(2);

end architecture smo_beh;
