library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library altera_mf;
use altera_mf.all;

entity alt_sqrt_kernel is
	generic	(
		INPUT_WIDTH	: natural;
		OUTPUT_WIDTH	: natural;
		PIPELINE_LENGTH	: natural
	);
	port	(
		CLOCK	: in std_logic;
		INPUT_SIGNAL	: in signed(INPUT_WIDTH-1 downto 0);
		OUTPUT_SIGNAL	: out signed(OUTPUT_WIDTH-1 downto 0)
	);
end entity alt_sqrt_kernel;

architecture beh of alt_sqrt_kernel is

	component altsqrt
		generic	(
			pipeline	 : natural;
			q_port_width : natural;
			r_port_width : natural;
			width		 : natural;
			lpm_type	 : string
		);
		port	(
			clk		  : in std_logic ;
			radical	  : in std_logic_vector(width-1 downto 0);
			q		  : out std_logic_vector(q_port_width-1 downto 0);
			remainder : out std_logic_vector(r_port_width-1 downto 0)
		);
	end component altsqrt;


	signal radical	: std_logic_vector(INPUT_SIGNAL'range) := (others => '0');
	signal result	: std_logic_vector(INPUT_SIGNAL'length/2-1 downto 0) := (others => '0');

begin

	radical <= std_logic_vector(INPUT_SIGNAL);

	ALTSQRT_component	: altsqrt
		generic map	(pipeline => PIPELINE_LENGTH, q_port_width => result'length, r_port_width => result'length+1, width => INPUT_WIDTH, lpm_type => "ALTSQRT")
		port map	(clk => CLOCK, radical => radical, q => result, remainder => open);

end architecture beh;
