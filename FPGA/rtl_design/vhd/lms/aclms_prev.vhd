library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library lib_complex;
use lib_complex.complex27.all;

library lib_VHDL;
use lib_VHDL.all;

entity ACLMS is
	generic
	(
		res		: natural := 27;
		Qbase	: natural := 21
	);
	port
	(
		clk, clks	: in std_logic;
		r	: in complex27;	-- Input
		est	: out complex27;
		--c0, c1, c2, c4, c5, c6	: out signed(res-1 downto 0)
		end_bit	: out std_logic
	);
end entity ACLMS;

architecture beh of ACLMS is

--	CORDIC
	component cordic is
		generic	(
			res	: natural := res;
			N	: natural := 21
		);
		port	(
			clk	: in std_logic;
			mode		: in natural range 0 to 3;		-- operation mode
			xi, yi, zi	: in signed(res-1 downto 0);	-- vector input
			xo, yo, zo	: out signed(res-1 downto 0)	-- vector output
		);
	end component cordic;

--	CORDIC Hyperbolic
	component cordic_hyp is
		generic	(
			res	: natural := res;
			N	: natural := 18
		);
		port	(
			clk	: in std_logic;
			mode		: in natural range 0 to 1;		-- operation mode
			xi, yi, zi	: in signed(res-1 downto 0);	-- vector input
			xo, yo, zo	: out signed(res-1 downto 0)	-- vector output
		);
	end component cordic_hyp;

--	FSM States
	type states is (hold, push, updt_lms, updt_atan, final);
	signal cs, ns	: states := hold;

--	FSM Signals
	signal start, end_alg	: std_logic := '0';
	signal load_input, load_lms, calc_atan	: std_logic := '0';

--	ACLMS Variables
	signal v, g, h, e, y, u	: complex27 := (others => (others => '0'));

--	Frequency Estimator Variables
	signal b, bsqrt	: complex27 := (others => (others => '0'));
	signal gmod, himag, hreal	: signed(res-1 downto 0) := (others => '0');
	signal a	: integer range 0 to 20 := 0;

--	CORDIC hyp variables
	--signal c0, c1, c2	: signed(res-1 downto 0) := (others => '0');

begin

	end_bit <= end_alg;

--	Start
	start_beh	: process(clk, clks, end_alg)
	begin
		if end_alg = '1' then
			start <= '0';
		elsif rising_edge(clk) and clks = '1' then
			start <= '1';
		end if;
	end process;

--	Finite State Machine
	FSM_updt	: process(clk, ns)
	begin
		if rising_edge(clk) then
			cs <= ns;
		end if;
	end process;

	FSM_BEH	: process(cs, start)
	begin

	--	Default State Value
		load_input <= '0';
		load_lms <= '0';
		end_alg <= '0';
		calc_atan <= '0';
	--	State Behaviour
		case cs is
			when hold	=>
				if start = '1' then
					ns <= push;
				else
					ns <= hold;
				end if;
			when push	=>
				load_input <= '1';
				ns <= updt_lms;
			when updt_lms	=>
				load_lms <= '1';
				ns <= updt_atan;
			when updt_atan	=>
				calc_atan <= '1';
				ns <= final;
			when final	=>
				end_alg <= '1';
				ns <= hold;
			when others	=>
				ns <= hold;
		end case;
	end process;


--	ACLMS Algorithm
	y <= mult(conj(h), v, Qbase) + mult(conj(g), conj(v), Qbase);

	e <= u - y;

--	ACLMS Flow
	process(clk, h, g, r, e, u, v, load_lms, load_input)
	begin
		if rising_edge(clk) then
			if load_input = '1' then
				--u <= shift_left(r, Qbase-18);
				--u <= r;
				u <= shift_left(cl_bits(r,0), Qbase-18);
				v <= u;
			end if;
			if load_lms = '1' then
				h <= h + shift_right(mult(conj(e), v, Qbase), 2);
				g <= g + shift_right(mult(conj(e), conj(v), Qbase), 2);
			end if;
		end if;
	end process;

	--est <= h;
	--a <= 8;	-- Qbase = 18
	a <= 14;	-- Qbase = 21
	gmod <= resize(shift_right(g(0)*g(0), a), res) + resize(shift_right(g(1)*g(1), a), res);	--	Q34
	himag <= resize(shift_right(h(1)*h(1), a), res);	--	Q34

	process(h, gmod, clk, calc_atan)
	begin
		if rising_edge(clk) and calc_atan = '1' then
--			b(0) <= himag - gmod + to_signed(65536, res);
--			b(1) <= himag - gmod - to_signed(65536, res);
			b(0) <= himag - gmod + to_signed(65536, res);
			b(1) <= himag - gmod - to_signed(65536, res);

		end if;
	end process;

--	Hyperbolic CORDIC
	CORDIC_hyp0	: cordic_hyp port map (clk, 1, b(0), b(1), (others => '0'), bsqrt(0), open, open);
	--bsqrt(1) <= resize(shift_right(bsqrt(0)*to_signed(316538,res), 18), res);
--	bsqrt(1) <= resize(shift_right(bsqrt(0)*to_signed(20218833,res), 24), res);
	bsqrt(1) <= resize(shift_right(bsqrt(0)*to_signed(20258439,res), 24), res);
--	hreal <= shift_left(h(0), 5);
	hreal <= shift_left(h(0), 2);
--	Estimate Frequency
	CORDIC0	: cordic port map (clk, 1, hreal, bsqrt(1), (others => '0'), open, open, est(0));

end architecture beh;
