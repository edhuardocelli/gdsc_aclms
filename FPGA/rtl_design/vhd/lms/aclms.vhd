library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library complex_type;
use complex_type.complex27.all;

entity ACLMS is
	generic	(
		DATA_WIDTH	: natural := 32;
		QN_BASE		: natural := 30
	);
	port 	(
		CLOCKn	: in std_logic;
		RESET	: in std_logic := '1';
		ENABLE	: in std_logic := '1';
		SIGNAL_INPUT	: in complex27
	);
end entity ACLMS;

architecture beh of ACLMS is

	component gdsc_reduced
		generic (
			DATA_WIDTH   : natural;
			QN_BASE      : natural;
			STAGE_LENGTH : natural
		);
		port (
			CLOCK          : in  std_logic;
			RESET          : in  std_logic;
			ENABLE         : in  std_logic;
			START_BIT      : in  std_logic;
			SIGNAL_ALPHA   : in  signed(DATA_WIDTH-1 downto 0);
			SIGNAL_BETA    : in  signed(DATA_WIDTH-1 downto 0);
			FILTERED_ALPHA : out signed(DATA_WIDTH-1 downto 0);
			FILTERED_BETA  : out signed(DATA_WIDTH-1 downto 0);
			END_BIT        : out std_logic
		);
	end component gdsc_reduced;


	component sqrt_rec_kernel
		generic (
			DATA_WIDTH      : natural;
			ITERATION       : natural;
			PIPELINE_LENGTH : natural;
			PRESCALE        : natural
		);
		port (
			CLOCK     : in  std_logic;
			RESET     : in  std_logic;
			ENABLE    : in  std_logic;
			START     : in  std_logic;
			RADICAL   : in  signed(DATA_WIDTH-1 downto 0);
			RADIX     : out signed(DATA_WIDTH-1 downto 0);
			REMAINDER : out signed(DATA_WIDTH-1 downto 0);
			END_BIT   : out std_logic
		);
	end component sqrt_rec_kernel;

	component cordic_rec
		generic (
			DATA_WIDTH : natural;
			QN_BASE    : natural;
			ITERATION  : natural;
			MODE       : std_logic
		);
		port (
			CLOCK        : in  std_logic;
			RESET        : in  std_logic;
			ENABLE       : in  std_logic;
			START        : in  std_logic;
			SIGNAL_IN_X  : in  signed(DATA_WIDTH-1 downto 0);
			SIGNAL_IN_Y  : in  signed(DATA_WIDTH-1 downto 0);
			SIGNAL_IN_Z  : in  signed(DATA_WIDTH-1 downto 0);
			SIGNAL_OUT_X : out signed(DATA_WIDTH-1 downto 0);
			SIGNAL_OUT_Y : out signed(DATA_WIDTH-1 downto 0);
			SIGNAL_OUT_Z : out signed(DATA_WIDTH-1 downto 0);
			END_BIT      : out std_logic
		);
	end component cordic_rec;



	signal clock, start : std_logic := '0';

--	Finite State Machine signals
	type states is (IDLE, GDSC_FILTER, PUSH, LMS_ALGORITHM, ROTATION_EST, FREQUENCY_EST, FINAL);
	signal fsm_cs, fsm_ns	: states := IDLE;
	signal hintx, flag	: std_logic_vector(5 downto 0) := (others => '0');

--	ACLMS Signals
	signal r, u, v, g, h, e, ubar	: complex27 := (others => (others => '0'));
	--signal h	: complex27 := tocomplex27(integer(2**real(QN_BASE-1)*cos(2*3.1415926538*50.0/100.0e3)), integer(2**real(QN_BASE-1)*sin(2*3.1415926538*50.0/100.0e3)));

--	Debug Signals
	signal alpha, beta	: integer range -2**(DATA_WIDTH-1) to 2**(DATA_WIDTH-1)-1 := 0;
	signal ang	: real := 0.0;

	signal b, radix_b, radix_b_scaled	: signed(DATA_WIDTH-1 downto 0) := (others => '0');
	signal omega_est, omega_est_reg	: signed(DATA_WIDTH-1 downto 0) := (others => '0');

begin

--	Input
	process
	begin
		clock <= not(clock) after 10 ns;
		wait for 10 ns;
	end process;

	process
	begin
		start <= not(start) after 3125 ns;
		wait for 3125 ns;
	end process;

--	Finite State Machine
	FSM_seq_logic	: process(CLOCK, ENABLE, RESET, fsm_ns)
	begin
		if RESET = '0' then
			fsm_cs <= IDLE;
		elsif rising_edge(CLOCK) and ENABLE = '1' then
			fsm_cs <= fsm_ns;
		end if;
	end process;

	FSM_comb_logic	: process(fsm_cs, start, flag)
	begin
		hintx <= (others => '0');

		case fsm_cs is
			when IDLE	=>
				if start = '1' then
					fsm_ns <= GDSC_FILTER;
				else
					fsm_ns <= IDLE;
				end if;

			when GDSC_FILTER	=>
				hintx(5) <= '1';
				if flag(2) <= '1' then
					fsm_ns <= PUSH;
				else
					fsm_ns <= GDSC_FILTER;
				end if;

			when PUSH	=>
				hintx(0) <= '1';
				fsm_ns <= LMS_ALGORITHM;

			when LMS_ALGORITHM	=>
				hintx(1) <= '1';
				fsm_ns <= ROTATION_EST;

			when ROTATION_EST	=>
				hintx(2) <= '1';
				if flag(0) = '1' then
					fsm_ns <= FREQUENCY_EST;
				else
					fsm_ns <= ROTATION_EST;
				end if;

			when FREQUENCY_EST	=>
				hintx(3) <= '1';
				if flag(1) = '1' then
					fsm_ns <= FINAL;
				else
					fsm_ns <= FREQUENCY_EST;
				end if;

			when FINAL	=>
				hintx(4) <= '1';
				if start = '1' then
					fsm_ns <= FINAL;
				else
					fsm_ns <= IDLE;
				end if;

			when others	=>
				fsm_ns <= IDLE;
		end case;
	end process;

--	Reference Generation
	--	Debug Alpha-Beta
	process(clock, start, ang)
	begin
		if rising_edge(start) then
			alpha <= integer(round(2.0**QN_BASE*cos(ang)));
			beta <= integer(round(2.0**QN_BASE*sin(ang)));
			if ang < 2*3.1415926538 then
				ang <= ang + 2*3.1415926538*50.0/160.0e3;
			else
				ang <= ang - 2*3.1415926538 + 2*3.1415926538*50.0/160.0e3;
			end if;
		end if;
	end process;
	r <= cl_bits(tocomplex27(alpha, beta), 18);
	--r <= tocomplex27(2**18,2**18);

--	GDSC Algorithm
gdsc_alg	: gdsc_reduced
	generic map	(DATA_WIDTH, QN_BASE, 5)
	port map	(clock, '1', '1', hintx(5), r(0), r(1), open, open, flag(2));

--	ACLMS Algorithm
	ubar <= mult(conj(h), v, QN_BASE) + mult(conj(g), conj(v), QN_BASE);
	e <= u - ubar;

	--	ACLMS Flow
	process(CLOCK, h, g, r, e, u, v, hintx)
	begin
		if rising_edge(CLOCK) then
			if hintx(0) = '1' then
				u <= r;
				v <= u;
			elsif hintx(1) = '1' then
				h <= h + shift_right(mult(conj(e), v, QN_BASE), 5);
				g <= g + shift_right(mult(conj(e), conj(v), QN_BASE), 5);
			end if;
		end if;
	end process;

	b <= resize(shift_right(h(1)*h(1) - g(0)*g(0) - g(1)*g(1), QN_BASE-10), DATA_WIDTH);

	sqrt_kernel	: sqrt_rec_kernel
		generic map	(32, 30, 3, 2**30/4)
		port map 	(clock, '1', '1', hintx(2), b, radix_b_scaled, open, flag(0));

	radix_b <= resize(shift_right(integer(2.0**QN_BASE/0.828159360960216)*radix_b_scaled, 35), DATA_WIDTH);

	atan2_calc	: cordic_rec
		generic map	(DATA_WIDTH, QN_BASE, 30, '1')
		port map	(clock, '1', '1', hintx(3), h(0), radix_b, (others => '0'), open, open, omega_est, flag(1));

	process(clock, hintx)
	begin
		if rising_edge(clock) and hintx(4) = '1' then
			omega_est_reg <= omega_est;
		end if;
	end process;


end architecture beh;
