library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.std_logic_unsigned.all;

entity moving_avg_pwm is
	generic	(
		DATA_WIDTH	: natural;
		MEMORY_WIDTH	: natural;
		SCALE_FACTOR	: natural
	);
	port 	(
		CLOCK 	: in std_logic;
		RESET	: in std_logic;
		ENABLE	: in std_logic;
		INPUT_SIGNAL	: in std_logic;
		AVERAGE	: out signed(DATA_WIDTH-1 downto 0)
	);
end entity moving_avg_pwm;

architecture beh of moving_avg_pwm is

	signal mem_buf	: std_logic_vector(MEMORY_WIDTH downto 0) := (others => '0');
	signal filter_output	: std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');

begin

	process(CLOCK, RESET, ENABLE, mem_buf)
	begin
		if RESET = '0' then
			mem_buf <= (others => '0');
			filter_output <= (others => '0');
		elsif rising_edge(CLOCK) and ENABLE = '1' then
			filter_output	<= filter_output + mem_buf(0) - mem_buf(MEMORY_WIDTH);
			for i in 0 to mem_buf'length-2 loop
				mem_buf(i+1) <= mem_buf(i);
			end loop;
			mem_buf(0) <= INPUT_SIGNAL;
		end if;
	end process;

	AVERAGE <= signed(filter_output);

end architecture beh;
