library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity debounce is
	generic	(
		RES	: natural;			--	Width of bus being debounced
		POLARITY	: std_logic;		--	'0' -> LOW / '1' => HIGH
		TIMEOUT	: natural;		--	Number of cycles the input signal must be HIGH or LOW
		TIMEOUT_WIDTH	: natural	--	ceil(log2(TIMEOUT))
	);
	port	(
		CLOCK	 : in std_logic;
		RESET	 : in std_logic;
		DATA_IN	 : in std_logic_vector(RES-1 downto 0);
		DATA_OUT : out std_logic_vector(RES-1 downto 0)
	);
end entity debounce;

architecture beh of debounce is

	type count is array(0 to DATA_IN'length-1) of unsigned(TIMEOUT_WIDTH-1 downto 0);
	signal counter	: count := (others => (others => '0'));
	signal data, counter_reset, counter_enable	: std_logic_vector(DATA_IN'range) := (others => '0');

begin

	process(CLOCK, counter, RESET, counter_reset, counter_enable)
	begin
		for i in 0 to DATA_IN'length-1 loop
			if rising_edge(CLOCK) then
				if RESET = '0' then
					counter(i) <= (others => '0');
				else
					if counter_reset(i) = '1' then
						counter(i) <= (others => '0');
					else
						if counter_enable(i) = '1' then
							counter(i) <= counter(i) + 1;
						end if;
					end if;
				end if;
			end if;
		end loop;
	end process;

	process(CLOCK, DATA_IN, counter)
	begin
		for i in 0 to DATA_IN'length-1 loop
			if rising_edge(CLOCK) then
				if DATA_IN(i) = not(POLARITY) then
					counter_reset(i) <= '1';
					counter_enable(i) <= '0';
					DATA_OUT(i) <= not(POLARITY);
				else
					counter_reset(i) <= '0';
					if counter(i) < TIMEOUT then
						counter_enable(i) <= '1';
						DATA_OUT(i) <= not(POLARITY);
					else
						counter_enable(i) <= '0';
						DATA_OUT(i) <= POLARITY;
					end if;
				end if;
			end if;
		end loop;
	end process;

end architecture beh;
