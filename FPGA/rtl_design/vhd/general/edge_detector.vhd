library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity edge_detector is
	generic	(
		POLARITY	: std_logic;
		TIME_DELAY	: natural
	);
	port	(
		CLOCK	: in std_logic;
		RESET 	: in std_logic;
		ENABLE 	: in std_logic;
		INPUT_SIGNAL	: in std_logic;
		OUTPUT_SIGNAL	: out std_logic
	);
end entity edge_detector;

architecture beh of edge_detector is

	constant time_width	: integer := integer(ceil(log(real(TIME_DELAY), 2.0)));
	signal time_counter	: unsigned(time_width-1 downto 0) := (others => '0');

	type fsm_states is (IDLE, EDGE, DELAY, BUSY);
	signal cs, ns	: fsm_states := IDLE;
	signal flag 	: std_logic := '0';

begin

	process(CLOCK, RESET, ns)
	begin
		if RESET = '0' then
			cs <= IDLE;
		elsif rising_edge(CLOCK) then
			cs <= ns;
		end if;
	end process;

	process(INPUT_SIGNAL, cs, time_counter)
	begin
		flag <= not(POLARITY);
		case cs is
			when IDLE 	=>
				if INPUT_SIGNAL = POLARITY then
					ns <= EDGE;
				else
					ns <= IDLE;
				end if;
			when EDGE	=>
				flag <= POLARITY;
				ns <= DELAY;
			when DELAY 	=>
				flag <= '1';
				if time_counter = TIME_DELAY-1 then
					ns <= BUSY;
				else
					ns <= DELAY;
				end if;
			when BUSY	=>
				if INPUT_SIGNAL = not(POLARITY) then
					ns <= IDLE;
				else
					ns <= BUSY;
				end if;
			when others	=>
				ns <= IDLE;
		end case;
	end process;

	process(CLOCK, RESET, ENABLE, time_counter, flag)
	begin
		if (RESET or ENABLE) = '0' then
			time_counter <= (others => '0');
		else
			if rising_edge(CLOCK) then
				if time_counter = TIME_DELAY-1 then
					time_counter <= (others => '0');
				else
					if flag = '0' then
						time_counter <= (others => '0');
					else
						time_counter <= time_counter + 1;
					end if;
				end if;
			end if;
		end if;
	end process;

	OUTPUT_SIGNAL <= flag;

end architecture beh;
