library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity FreeRTOS is
	port	(
		--	Clocks
		FPGA_CLK1_50, FPGA_CLK2_50, FPGA_CLK3_50	: std_logic := '0';
		--	ADC
		ADC_SDO	: in std_logic := '0';
		ADC_CONVST, ADC_SCK, ADC_SDI	: out std_logic;
		--	HPS
			--	USB
		HPS_CONV_USB_N	: inout std_logic;
			--	DDR3 RAM
		HPS_DDR3_ADDR	: out std_logic_vector(14 downto 0);
		HPS_DDR3_BA	: out std_logic_vector(2 downto 0);
		HPS_DDR3_CAS_N, HPS_DDR3_CK_N, HPS_DDR3_CK_P, HPS_DDR3_CKE, HPS_DDR3_CS_N	: out std_logic;
		HPS_DDR3_DM	: out std_logic_vector(3 downto 0);
		HPS_DDR3_DQ	: inout std_logic_vector(31 downto 0);
		HPS_DDR3_DQS_N, HPS_DDR3_DQS_P 	: inout std_logic_vector(3 downto 0);
		HPS_DDR3_ODT, HPS_DDR3_RAS_N, HPS_DDR3_RESET_N	: out std_logic;
		HPS_DDR3_RZQ	: in std_logic;
		HPS_DDR3_WE_N	: out std_logic;
			--	ETHERNET
		HPS_ENET_GTX_CLK	: out std_logic;
		HPS_ENET_INT_N, HPS_ENET_MDIO	: inout std_logic;
		HPS_ENET_MDC, HPS_ENET_TX_EN	: out std_logic;
		HPS_ENET_RX_CLK, HPS_ENET_RX_DV	: in std_logic;
		HPS_ENET_RX_DATA	: in std_logic_vector(3 downto 0);
		HPS_ENET_TX_DATA	: out std_logic_vector(3 downto 0);
			--	GSENSOR
		HPS_GSENSOR_INT	: inout std_logic;
			--	I2C
		HPS_I2C0_SCLK, HPS_I2C0_SDAT, HPS_I2C1_SCLK, HPS_I2C1_SDAT	: inout std_logic;
			--	DEBUG
		HPS_KEY, HPS_LED, HPS_LTC_GPIO	: inout std_logic;
			--	SD CARD
		HPS_SD_CLK 	: out std_logic;
		HPS_SD_CMD	: inout std_logic;
		HPS_SD_DATA	: inout std_logic_vector(3 downto 0);
			--	SPI
		HPS_SPIM_CLK, HPS_SPIM_MOSI 	: out std_logic;
		HPS_SPIM_MISO	: in std_logic;
		HPS_SPIM_SS	: inout std_logic;
			--	UART
		HPS_UART_RX	: in std_logic;
		HPS_UART_TX	: out std_logic;
			--	USB
		HPS_USB_CLKOUT, HPS_USB_DIR, HPS_USB_NXT	: in std_logic;
		HPS_USB_DATA	: inout std_logic_vector(7 downto 0);
		HPS_USB_STP	: out std_logic;
			--	FLASH
	--	HPS_FLASH_DATA	: inout std_logic_vector(3 downto 0);
	--	HPS_FLASH_DCLK, HPS_FLASH_NCSO	: out std_logic;
		--	DEBUG
		KEY	: in unsigned(1 downto 0);
		SW	: in unsigned(3 downto 0);
		LED	: out std_logic_vector(7 downto 0);
		GPIO_0	: in std_logic_vector(35 downto 0);
		GPIO_1	: out std_logic_vector(35 downto 0)
	);

end entity FreeRTOS;

architecture beh of FreeRTOS is

--	HPS System
	component hps is
		port (
			clk_clk                            : in    std_logic                     := 'X';             -- clk
			hps_ddr3_memory_mem_a              : out   std_logic_vector(14 downto 0);                    -- mem_a
			hps_ddr3_memory_mem_ba             : out   std_logic_vector(2 downto 0);                     -- mem_ba
			hps_ddr3_memory_mem_ck             : out   std_logic;                                        -- mem_ck
			hps_ddr3_memory_mem_ck_n           : out   std_logic;                                        -- mem_ck_n
			hps_ddr3_memory_mem_cke            : out   std_logic;                                        -- mem_cke
			hps_ddr3_memory_mem_cs_n           : out   std_logic;                                        -- mem_cs_n
			hps_ddr3_memory_mem_ras_n          : out   std_logic;                                        -- mem_ras_n
			hps_ddr3_memory_mem_cas_n          : out   std_logic;                                        -- mem_cas_n
			hps_ddr3_memory_mem_we_n           : out   std_logic;                                        -- mem_we_n
			hps_ddr3_memory_mem_reset_n        : out   std_logic;                                        -- mem_reset_n
			hps_ddr3_memory_mem_dq             : inout std_logic_vector(31 downto 0) := (others => 'X'); -- mem_dq
			hps_ddr3_memory_mem_dqs            : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs
			hps_ddr3_memory_mem_dqs_n          : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs_n
			hps_ddr3_memory_mem_odt            : out   std_logic;                                        -- mem_odt
			hps_ddr3_memory_mem_dm             : out   std_logic_vector(3 downto 0);                     -- mem_dm
			hps_ddr3_memory_oct_rzqin          : in    std_logic                     := 'X';             -- oct_rzqin
			hps_f2h_cold_reset_req_reset_n     : in    std_logic                     := 'X';             -- reset_n
			hps_f2h_debug_reset_req_reset_n    : in    std_logic                     := 'X';             -- reset_n
			hps_f2h_stm_hw_events_stm_hwevents : in    std_logic_vector(27 downto 0) := (others => 'X'); -- stm_hwevents
			hps_f2h_warm_reset_req_reset_n     : in    std_logic                     := 'X';             -- reset_n
			hps_h2f_reset_reset_n              : out   std_logic;                                        -- reset_n
			hps_io_hps_io_emac1_inst_TX_CLK    : out   std_logic;                                        -- hps_io_emac1_inst_TX_CLK
			hps_io_hps_io_emac1_inst_TXD0      : out   std_logic;                                        -- hps_io_emac1_inst_TXD0
			hps_io_hps_io_emac1_inst_TXD1      : out   std_logic;                                        -- hps_io_emac1_inst_TXD1
			hps_io_hps_io_emac1_inst_TXD2      : out   std_logic;                                        -- hps_io_emac1_inst_TXD2
			hps_io_hps_io_emac1_inst_TXD3      : out   std_logic;                                        -- hps_io_emac1_inst_TXD3
			hps_io_hps_io_emac1_inst_RXD0      : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RXD0
			hps_io_hps_io_emac1_inst_MDIO      : inout std_logic                     := 'X';             -- hps_io_emac1_inst_MDIO
			hps_io_hps_io_emac1_inst_MDC       : out   std_logic;                                        -- hps_io_emac1_inst_MDC
			hps_io_hps_io_emac1_inst_RX_CTL    : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RX_CTL
			hps_io_hps_io_emac1_inst_TX_CTL    : out   std_logic;                                        -- hps_io_emac1_inst_TX_CTL
			hps_io_hps_io_emac1_inst_RX_CLK    : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RX_CLK
			hps_io_hps_io_emac1_inst_RXD1      : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RXD1
			hps_io_hps_io_emac1_inst_RXD2      : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RXD2
			hps_io_hps_io_emac1_inst_RXD3      : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RXD3
	--		hps_io_hps_io_qspi_inst_IO0        : inout std_logic                     := 'X';             -- hps_io_qspi_inst_IO0
	--		hps_io_hps_io_qspi_inst_IO1        : inout std_logic                     := 'X';             -- hps_io_qspi_inst_IO1
	--		hps_io_hps_io_qspi_inst_IO2        : inout std_logic                     := 'X';             -- hps_io_qspi_inst_IO2
	--		hps_io_hps_io_qspi_inst_IO3        : inout std_logic                     := 'X';             -- hps_io_qspi_inst_IO3
	--		hps_io_hps_io_qspi_inst_SS0        : out   std_logic;                                        -- hps_io_qspi_inst_SS0
	--		hps_io_hps_io_qspi_inst_CLK        : out   std_logic;                                        -- hps_io_qspi_inst_CLK
			hps_io_hps_io_sdio_inst_CMD        : inout std_logic                     := 'X';             -- hps_io_sdio_inst_CMD
			hps_io_hps_io_sdio_inst_D0         : inout std_logic                     := 'X';             -- hps_io_sdio_inst_D0
			hps_io_hps_io_sdio_inst_D1         : inout std_logic                     := 'X';             -- hps_io_sdio_inst_D1
			hps_io_hps_io_sdio_inst_CLK        : out   std_logic;                                        -- hps_io_sdio_inst_CLK
			hps_io_hps_io_sdio_inst_D2         : inout std_logic                     := 'X';             -- hps_io_sdio_inst_D2
			hps_io_hps_io_sdio_inst_D3         : inout std_logic                     := 'X';             -- hps_io_sdio_inst_D3
			hps_io_hps_io_usb1_inst_D0         : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D0
			hps_io_hps_io_usb1_inst_D1         : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D1
			hps_io_hps_io_usb1_inst_D2         : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D2
			hps_io_hps_io_usb1_inst_D3         : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D3
			hps_io_hps_io_usb1_inst_D4         : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D4
			hps_io_hps_io_usb1_inst_D5         : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D5
			hps_io_hps_io_usb1_inst_D6         : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D6
			hps_io_hps_io_usb1_inst_D7         : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D7
			hps_io_hps_io_usb1_inst_CLK        : in    std_logic                     := 'X';             -- hps_io_usb1_inst_CLK
			hps_io_hps_io_usb1_inst_STP        : out   std_logic;                                        -- hps_io_usb1_inst_STP
			hps_io_hps_io_usb1_inst_DIR        : in    std_logic                     := 'X';             -- hps_io_usb1_inst_DIR
			hps_io_hps_io_usb1_inst_NXT        : in    std_logic                     := 'X';             -- hps_io_usb1_inst_NXT
			hps_io_hps_io_spim1_inst_CLK       : out   std_logic;                                        -- hps_io_spim1_inst_CLK
			hps_io_hps_io_spim1_inst_MOSI      : out   std_logic;                                        -- hps_io_spim1_inst_MOSI
			hps_io_hps_io_spim1_inst_MISO      : in    std_logic                     := 'X';             -- hps_io_spim1_inst_MISO
			hps_io_hps_io_spim1_inst_SS0       : out   std_logic;                                        -- hps_io_spim1_inst_SS0
			hps_io_hps_io_uart0_inst_RX        : in    std_logic                     := 'X';             -- hps_io_uart0_inst_RX
			hps_io_hps_io_uart0_inst_TX        : out   std_logic;                                        -- hps_io_uart0_inst_TX
			hps_io_hps_io_i2c0_inst_SDA        : inout std_logic                     := 'X';             -- hps_io_i2c0_inst_SDA
			hps_io_hps_io_i2c0_inst_SCL        : inout std_logic                     := 'X';             -- hps_io_i2c0_inst_SCL
			hps_io_hps_io_i2c1_inst_SDA        : inout std_logic                     := 'X';             -- hps_io_i2c1_inst_SDA
			hps_io_hps_io_i2c1_inst_SCL        : inout std_logic                     := 'X';             -- hps_io_i2c1_inst_SCL
			hps_io_hps_io_gpio_inst_GPIO09     : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO09
			hps_io_hps_io_gpio_inst_GPIO35     : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO35
			hps_io_hps_io_gpio_inst_GPIO40     : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO40
			hps_io_hps_io_gpio_inst_GPIO53     : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO53
			hps_io_hps_io_gpio_inst_GPIO54     : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO54
			hps_io_hps_io_gpio_inst_GPIO61     : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO61
			fpga_led_export                    : out   std_logic_vector(7 downto 0);                     -- export
			irq_export                         : in    std_logic_vector(7 downto 0)  := (others => 'X'); -- export
			p_in_export                        : in    std_logic_vector(31 downto 0) := (others => 'X'); -- export
			p_out_export                       : out   std_logic_vector(31 downto 0);                    -- export
			reset_reset_n                      : in    std_logic                     := 'X'              -- reset_n
		);
	end component hps;

	constant HPS_IRQ	: std_logic_vector(7 downto 0) := (others => '0');
	signal HPS_P_IN, HPS_P_OUT 	: std_logic_vector(31 downto 0) := (others => '0');
	constant HPS_COLD_RESET, HPS_DEBUG_RESET, HPS_WARM_RESET	: std_logic := '0';
	signal HPS_FPGA_RESET_N	: std_logic := '0';
	signal STM_HW_EVENTS 	: std_logic_vector(27 downto 0) := (others => '0');

begin

u0	:	component hps
	port map (
    		--	Clock
		clk_clk                            => FPGA_CLK1_50,		--	Clock (50 MHz)
		--	DDR3 Memory
		hps_ddr3_memory_mem_a              => HPS_DDR3_ADDR,
		hps_ddr3_memory_mem_ba             => HPS_DDR3_BA,
		hps_ddr3_memory_mem_ck             => HPS_DDR3_CK_P,
		hps_ddr3_memory_mem_ck_n           => HPS_DDR3_CK_N,
		hps_ddr3_memory_mem_cke            => HPS_DDR3_CKE,
		hps_ddr3_memory_mem_cs_n           => HPS_DDR3_CS_N,
		hps_ddr3_memory_mem_ras_n          => HPS_DDR3_RAS_N,
		hps_ddr3_memory_mem_cas_n          => HPS_DDR3_CAS_N,
		hps_ddr3_memory_mem_we_n           => HPS_DDR3_WE_N,
		hps_ddr3_memory_mem_reset_n        => HPS_DDR3_RESET_N,
		hps_ddr3_memory_mem_dq             => HPS_DDR3_DQ,
		hps_ddr3_memory_mem_dqs            => HPS_DDR3_DQS_P,
		hps_ddr3_memory_mem_dqs_n          => HPS_DDR3_DQS_N,
		hps_ddr3_memory_mem_odt            => HPS_DDR3_ODT,
		hps_ddr3_memory_mem_dm             => HPS_DDR3_DM,
		hps_ddr3_memory_oct_rzqin          => HPS_DDR3_RZQ,
		--	Baremetal Bus
		hps_f2h_cold_reset_req_reset_n     => not(HPS_COLD_RESET),
		hps_f2h_debug_reset_req_reset_n    => not(HPS_DEBUG_RESET),
		hps_f2h_stm_hw_events_stm_hwevents => STM_HW_EVENTS,
		hps_f2h_warm_reset_req_reset_n     => not(HPS_WARM_RESET),
		hps_h2f_reset_reset_n              => HPS_FPGA_RESET_N,
		--	Ethernet
		hps_io_hps_io_emac1_inst_TX_CLK    => HPS_ENET_GTX_CLK,
		hps_io_hps_io_emac1_inst_TXD0      => HPS_ENET_TX_DATA(0),
		hps_io_hps_io_emac1_inst_TXD1      => HPS_ENET_TX_DATA(1),
		hps_io_hps_io_emac1_inst_TXD2      => HPS_ENET_TX_DATA(2),
		hps_io_hps_io_emac1_inst_TXD3      => HPS_ENET_TX_DATA(3),
		hps_io_hps_io_emac1_inst_RXD0      => HPS_ENET_RX_DATA(0),
		hps_io_hps_io_emac1_inst_RXD1      => HPS_ENET_RX_DATA(1),
		hps_io_hps_io_emac1_inst_RXD2      => HPS_ENET_RX_DATA(2),
		hps_io_hps_io_emac1_inst_RXD3      => HPS_ENET_RX_DATA(3),
		hps_io_hps_io_emac1_inst_MDIO      => HPS_ENET_MDIO,
		hps_io_hps_io_emac1_inst_MDC       => HPS_ENET_MDC,
		hps_io_hps_io_emac1_inst_RX_CTL    => HPS_ENET_RX_DV,
		hps_io_hps_io_emac1_inst_TX_CTL    => HPS_ENET_TX_EN,
		hps_io_hps_io_emac1_inst_RX_CLK    => HPS_ENET_RX_CLK,
		--	QSPI (Flash Memory)
--		hps_io_hps_io_qspi_inst_IO0        => HPS_FLASH_DATA(0),
--		hps_io_hps_io_qspi_inst_IO1        => HPS_FLASH_DATA(1),
--		hps_io_hps_io_qspi_inst_IO2        => HPS_FLASH_DATA(2),
--		hps_io_hps_io_qspi_inst_IO3        => HPS_FLASH_DATA(3),
--		hps_io_hps_io_qspi_inst_SS0        => HPS_FLASH_NCSO,
--		hps_io_hps_io_qspi_inst_CLK        => HPS_FLASH_DCLK,
		--	SD Card
		hps_io_hps_io_sdio_inst_CMD        => HPS_SD_CMD,
		hps_io_hps_io_sdio_inst_D0         => HPS_SD_DATA(0),
		hps_io_hps_io_sdio_inst_D1         => HPS_SD_DATA(1),
		hps_io_hps_io_sdio_inst_D2         => HPS_SD_DATA(2),
		hps_io_hps_io_sdio_inst_D3         => HPS_SD_DATA(3),
		hps_io_hps_io_sdio_inst_CLK        => HPS_SD_CLK,
		--	USB
		hps_io_hps_io_usb1_inst_D0         => HPS_USB_DATA(0),
		hps_io_hps_io_usb1_inst_D1         => HPS_USB_DATA(1),
		hps_io_hps_io_usb1_inst_D2         => HPS_USB_DATA(2),
		hps_io_hps_io_usb1_inst_D3         => HPS_USB_DATA(3),
		hps_io_hps_io_usb1_inst_D4         => HPS_USB_DATA(4),
		hps_io_hps_io_usb1_inst_D5         => HPS_USB_DATA(5),
		hps_io_hps_io_usb1_inst_D6         => HPS_USB_DATA(6),
		hps_io_hps_io_usb1_inst_D7         => HPS_USB_DATA(7),
		hps_io_hps_io_usb1_inst_CLK        => HPS_USB_CLKOUT,
		hps_io_hps_io_usb1_inst_STP        => HPS_USB_STP,
		hps_io_hps_io_usb1_inst_DIR        => HPS_USB_DIR,
		hps_io_hps_io_usb1_inst_NXT        => HPS_USB_NXT,
		--	SPI1
		hps_io_hps_io_spim1_inst_CLK       => HPS_SPIM_CLK,
		hps_io_hps_io_spim1_inst_MOSI      => HPS_SPIM_MOSI,
		hps_io_hps_io_spim1_inst_MISO      => HPS_SPIM_MISO,
		hps_io_hps_io_spim1_inst_SS0       => HPS_SPIM_SS,
		--	UART
		hps_io_hps_io_uart0_inst_RX        => HPS_UART_RX,
		hps_io_hps_io_uart0_inst_TX        => HPS_UART_TX,
		--	I2C
		hps_io_hps_io_i2c0_inst_SDA        => HPS_I2C0_SDAT,
		hps_io_hps_io_i2c0_inst_SCL        => HPS_I2C0_SCLK,
		hps_io_hps_io_i2c1_inst_SDA        => HPS_I2C1_SDAT,
		hps_io_hps_io_i2c1_inst_SCL        => HPS_I2C1_SCLK,
		--	HPS GPIOs
		hps_io_hps_io_gpio_inst_GPIO09	=> HPS_CONV_USB_N,
		hps_io_hps_io_gpio_inst_GPIO35	=> HPS_ENET_INT_N,
		hps_io_hps_io_gpio_inst_GPIO40	=> HPS_LTC_GPIO,
		hps_io_hps_io_gpio_inst_GPIO53	=> HPS_LED,
		hps_io_hps_io_gpio_inst_GPIO54	=> HPS_KEY,
		hps_io_hps_io_gpio_inst_GPIO61	=> HPS_GSENSOR_INT,
		--	FPGA LEDs
		fpga_led_export				=> LED,
		--	External Interrupt Vector (8 Bits)
		irq_export                         => HPS_IRQ,
		--	Input Port (32 Bits)
		p_in_export                        => HPS_P_IN,
		p_out_export                       => HPS_P_OUT,
		reset_reset_n                      => '1'
	);

	HPS_P_IN	<= GPIO_0(31 downto 0);
	GPIO_1 	<= x"0"& HPS_P_OUT;
	--LED <= to_unsigned(97, 8);

end architecture beh;
