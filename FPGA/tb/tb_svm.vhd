library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library lib_VHDL;
use lib_VHDL.all;

entity tb is
end entity tb;

architecture tb_beh of tb is

	component svm
		generic (
			DATA_WIDTH	: natural;
			CLOCK_FREQUENCY	: real;
			SAMPL_FREQUENCY	: real;
			QN_BASE	: natural
		);
		port (
			CLOCK	: in std_logic;
			RESET	: in std_logic;
			ENABLE	: in std_logic;
			START_BIT	: in std_logic;
			ALPHA	: in signed(DATA_WIDTH-1 downto 0);
			BETA	: in signed(DATA_WIDTH-1 downto 0);
			SWITCHES: out std_logic_vector(2 downto 0);
			INTX 	: out std_logic
		);
	end component svm;

	constant t_step	: integer := 2059;
	constant Tclk	: time := 20 ns;
	constant Tsamp	: time := 25 us;
	signal clk, rst, en, start	: std_logic := '0';
	signal ang	: signed(26 downto 0) := (others => '0');

	signal wt	: real := 0.0;
	constant del_wt	: real := 0.007853981633974;
	signal al, be	: signed(26 downto 0) := (others => '0');

begin

	rst <= '1';
	en <= '1';

	process
	begin
		clk <= '0';
		wait for Tclk/2;
		clk <= '1';
		wait for Tclk/2;
	end process;

	process
	begin
		start <= '0';
		wait for Tsamp/2;
		start <= '1';
		wait for Tsamp/2;
	end process;

	process(start, ang)
	begin
		if rising_edge(start) then
			ang <= ang+t_step;
		end if;
	end process;

	process(clk, wt)
	begin
		if rising_edge(start) then
			wt <= wt + del_wt;
		end if;
	end process;

	al <= to_signed(integer(0.5*cos(wt)*(2.0**18.0)), 27);
	be <= to_signed(integer(0.5*sin(wt)*(2.0**18.0)), 27);

	svm0	: svm
	generic map	(27,50.0e6,40.0e3,18)
	port map	(clk,rst,en,start,al,be,open, open);

	

end architecture tb_beh;
