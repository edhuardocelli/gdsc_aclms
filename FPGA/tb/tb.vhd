library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library lib_VHDL;
use lib_VHDL.all;

entity tb is
end entity tb;

architecture RTL of tb is

	component hil_boost is
		generic	(
			RES	: natural := 27;
			QRES	: natural := 18
		);
		port 	(
			clock, enable	: in std_logic;
			s 	: in std_logic_vector(1 downto 0);
			v_l	: in signed(RES-1 downto 0);
			i_f, v_c, i_l	: out signed(RES-1 downto 0)
		);
	end component hil_boost;

	component hil_boost_avg is
		generic	(
			RES	: natural := 27;
			QRES	: natural := 18
		);
		port 	(
			clock, enable	: in std_logic;
			v_f, v_l	: in signed(RES-1 downto 0);
			i_f, v_c, i_l	: out signed(RES-1 downto 0)
		);
	end component hil_boost_avg;

	component smo is
		generic	(
			RES	: natural := 27;
			QRES	: natural := 18
		);
		port 	(
			clock, enable	: in std_logic;
			v_f, v_l	: in signed(RES-1 downto 0);
			v_c, i_l	: in signed(RES-1 downto 0);
			i_f	: out signed(RES-1 downto 0)
		);
	end component smo;

	component sympwm is
		generic	(
			RES	: natural;
			MAX_COUNTER	: natural;
			INT_GEN	: natural
		);
		port	(
			clock, enable	: in std_logic;
			ref_A, ref_B	: in unsigned(RES-1 downto 0);
			pulse_A, pulse_B, intx 	: out std_logic
		);
	end component sympwm;

	component ssfb is
		generic	(
			RES	: natural := 27;
			QRES	: natural := 18;
			QRESw	: natural := 12
		);
		port	(
			clock, enable	: in std_logic;
			r, fb	: in signed(RES-1 downto 0);
			x1, x2, x3	: in signed(RES-1 downto 0);
			y 	: out signed(RES-1 downto 0)
		);
	end component ssfb;

	component FreeRTOS is
		generic	(
			RES	: natural := 27;
			PWM_MAX	: natural := 2500
		);
		port	(
			--	Clocks
			FPGA_CLK1_50, FPGA_CLK2_50, FPGA_CLK3_50	: std_logic := '0';
			--	ADC
			ADC_SDO	: in std_logic := '0';
			ADC_CONVST, ADC_SCK, ADC_SDI	: out std_logic;
			--	DEBUG
			KEY	: in unsigned(1 downto 0);
			SW	: in unsigned(3 downto 0);
			LED	: out std_logic_vector(7 downto 0);
			GPIO_0	: out std_logic_vector(35 downto 0);
			GPIO_1	: out std_logic_vector(35 downto 0)
		);
	end component FreeRTOS;


	signal Tc	: time := 20 ns;

	signal Ta	: time := 1000 ns;
	signal clk, clks, clkn	: std_logic := '0';

	signal r, u, i_f, v_c, i_l	: signed(26 downto 0) := (others => '0');
	signal sine, t, theta 	: real := 0.0;
begin

	process
	begin
		t <= t + 0.000025;
		wait for Tc;
	end process;

	theta <= 2.0*MATH_PI*50.0*t;
	sine <= 5.0*(sin(theta))*262144.0;
	--u <= to_signed(262144, 27);
	r <= to_signed(integer(sine), 27);

rtos0	: FreeRTOS port map(clk, '0', '0', '0', open, open, open, "11", "0000", open, open, open);

--	boostavg0	: hil_boost_avg port map (clk, '1', u, (others => '0'), open, v_c, i_l);
	--smo0	: smo port map(clk, '1', u, (others => '0'), v_c, i_l, i_f);
	--feedback	: ssfb port map (clk, '1', r, i_l, i_l, v_c, i_f, u);

	process
	begin
		clk <= not(clk) after Tc/2;
		wait for Tc/2;
	end process;
	clkn <= not(clk);
	process
	begin
		clks <= not(clks) after Ta/2;
		wait for Ta/2;
	end process;

end architecture RTL;
