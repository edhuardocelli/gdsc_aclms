library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package lib_opal is
	constant DATA_WIDTH		: natural := 12;
	type opal_data_type is array(natural range <>) of std_logic_vector(DATA_WIDTH-1 downto 0);
end package lib_opal;
