library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package complex27 is

	-- Complex Type
	constant DATA_WIDTH	: natural := 32;
	type complex27 is array(0 to 1) of signed(DATA_WIDTH-1 downto 0);

	-- Operations
	function "+"	(L, R	: complex27) return complex27;
	function "-"	(L, R	: complex27) return complex27;
	function mult	(L, R	: complex27; base	: natural) return complex27;
	function abs2	(ARG	: complex27; base	: natural) return signed;
	function conj	(ARG	: complex27) return complex27;
	function shift_left		(ARG	: complex27; n	: natural) return complex27;
	function shift_right	(ARG	: complex27; n	: natural) return complex27;
	function tocomplex27	(L, R	: integer) return complex27;
	function cl_bits (ARG	: complex27; n	: natural) return complex27;

end complex27;

package body complex27 is

	function "+" (L, R	: complex27) return complex27 is
	begin
		return complex27'(L(0) + R(0), L(1) + R(1));
	end function "+";

	function "-" (L, R	: complex27) return complex27 is
	begin
		return complex27'(L(0) - R(0), L(1) - R(1));
	end function "-";

	function mult (L, R	: complex27; base	: natural) return complex27 is
	begin
		return complex27'(resize(shift_right(L(0)*R(0) - L(1)*R(1), base), L(0)'length),
						resize(shift_right(L(0)*R(1) + L(1)*R(0), base), L(0)'length));
	end function mult;

	function abs2 (ARG	: complex27; base	: natural) return signed is
	begin
		return resize(shift_right(ARG(0)*ARG(0) + ARG(1)*ARG(1), base), ARG(0)'length);
	end function abs2;

	function conj (ARG	: complex27) return complex27 is
	begin
		return (ARG(0), -ARG(1));
	end function conj;

	function shift_right (ARG	: complex27; n	: natural) return complex27 is
	begin
		return (shift_right(ARG(0), n), shift_right(ARG(1), n));
	end function shift_right;

	function shift_left (ARG	: complex27; n	: natural) return complex27 is
	begin
		return (shift_left(ARG(0), n), shift_left(ARG(1), n));
	end function shift_left;

	function tocomplex27 (L,R	: integer) return complex27 is
	begin
		return (to_signed(L, DATA_WIDTH), to_signed(R, DATA_WIDTH));
	end function tocomplex27;

	function cl_bits (ARG	: complex27; n	: natural) return complex27 is
		variable temp	: complex27 := (others => (others => '0'));
	begin
		for i in 0 to 1 loop
			temp(i)(DATA_WIDTH-1 downto n) := ARG(i)(DATA_WIDTH-1 downto n);
			temp(i)(n-1 downto 0) := (others => '0');
		end loop;
		return temp;
	end function cl_bits;

end package body complex27;
