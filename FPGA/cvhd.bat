::	Create File Library
vdel -lib  %PATH_WORK%\lib\lib_OPAL -all
vlib %PATH_WORK%\lib\lib_OPAL
vmap opal %PATH_WORK%\lib\lib_OPAL

::	Create File Library
vdel -lib  %PATH_WORK%\lib\lib_complex -all
vlib %PATH_WORK%\lib\lib_complex
vmap complex_type %PATH_WORK%\lib\lib_complex


::	Create File Library
vdel -lib  %PATH_WORK%\lib\lib_VHDL -all
vlib %PATH_WORK%\lib\lib_VHDL
vmap lib_VHDL %PATH_WORK%\lib\lib_VHDL

::	Create Testbench Libraries
vdel -lib  %PATH_WORK%\lib\lib_TB -all
vlib %PATH_WORK%\lib\lib_TB
vmap lib_TB %PATH_WORK%\lib\lib_TB

::	Compile VHDL Files
	::	Library Files
vcom -work complex_type .\lib\complex_type\complex_lib.vhd

	::	General Use Components
vcom -work lib_VHDL .\rtl_design\vhd\general\edge_detector.vhd
vcom -work lib_VHDL .\rtl_design\vhd\general\both_edge_detector.vhd
vcom -work lib_VHDL .\rtl_design\vhd\general\debounce.vhd
vcom -work lib_VHDL .\rtl_design\vhd\general\moving_avg_pwm.vhd

::	CORDIC
	::	CORDIC Pipelined
	vcom -work lib_VHDL .\rtl_design\vhd\cordic\cordic_it.vhd
	vcom -work lib_VHDL .\rtl_design\vhd\cordic\cordic.vhd
	::	CORDIC Circular Recursive
	vcom -work lib_VHDL .\rtl_design\vhd\cordic\circ\cordic_it_rec.vhd
	vcom -work lib_VHDL .\rtl_design\vhd\cordic\circ\cordic_rec.vhd
	::	CORDIC Hyperbolic Recursive
	::vcom -work lib_VHDL .\rtl_design\vhd\cordic\hyp\cordic_hyp_it_rec.vhd
	::vcom -work lib_VHDL .\rtl_design\vhd\cordic\hyp\cordic_hyp_rec.vhd
	::	SQRT Recursive Kernel
	vcom -work lib_VHDL .\rtl_design\vhd\cordic\hyp\sqrt_kernel_it.vhd
	vcom -work lib_VHDL .\rtl_design\vhd\cordic\hyp\sqrt_kernel.vhd
	vcom -work lib_VHDL .\rtl_design\vhd\cordic\hyp\sqrt_rec_kernel.vhd
::	ALTERA Functions
::vcom -work lib_VHDL .\rtl_design\vhd\ip\sqrt\alt_sqrt_kernel.vhd

::	LMS
vcom -work lib_VHDL .\rtl_design\vhd\gdsc\gdsc_memory.vhd
vcom -work lib_VHDL .\rtl_design\vhd\gdsc\gdsc_reduced.vhd
vcom -work lib_VHDL .\rtl_design\vhd\lms\aclms.vhd
::	SVPWM Modulation
::vcom -work lib_VHDL .\rtl_design\vhd\svm\sv_modulator.vhd
::vcom -work lib_VHDL .\rtl_design\vhd\svm\svm.vhd
	::	OPAL-RT Communication
::vcom -work opal .\lib\opal\opal_lib.vhd
::vcom -work lib_VHDL .\rtl_design\vhd\opal\opal_driver.vhd
::vcom -work lib_VHDL .\rtl_design\vhd\opal\opal_spi.vhd
	::	Top Entity
::vcom -work lib_VHDL .\rtl_design\vhd\top\FreeRTOS.vhd

::vcom -work lib_TB .\tb\tb_svm.vhd
