clear
close all
clc

%%

h = 1/160e3;
h_lms = h*10;
fgrid = 50;
delay = [1/2 1/4 1/8 1/16 1/32]';
k = [2 3 5 9 17].';
p = (delay./(h*fgrid))
Omeg = 2*pi*fgrid*h;
N = 1/(fgrid*h);

%%
a = 1./(1-exp(1i*(k-1).*Omeg.*p));
b = -a.*exp(1i*k.*Omeg.*p);
rot_angle = [angle(a) angle(b)]
%%

qn_base = 18;
cordic_it = qn_base + 1;
cordic_gain = 1;
for i = 0:cordic_it-1
    cordic_gain = cordic_gain/sqrt(1 + 2^(-2*i));
end
cordic_angle = round(2^qn_base*atan(2.^-(0:cordic_it-1)));

%%
for i = 1:length(cordic_angle)
    x(i) = cordic_angle(i) - round(cordic_angle(1)/2^(i-1));
end



